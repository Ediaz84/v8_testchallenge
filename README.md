## V8 Test Challenge
Este test challenge consta de 3 partes, el FrontEnd, el BackEnd y el proyecto de testing.

# BackEnd
Web Api 2 con Net 5, siguiendo principios DDD

# FrontEnd
MVC Core con Net 5, todas las peticiones van hacia la Web Api.

# Testing
Xunit con libreria Moq.

* Se adjunta un archivo script con las tablas y datos de prueba, la ubicación del archivo está en la carpeta FrontEnd dentro de
  la carpeta BD_Script.
* Se uso Sql Server 2014 como Base de Datos.
* Para que el aplicativo funcione primero debe ejecutar el proyecto BackEnd (click derecho al proyecto debug/new instance) y 
  luego el proyecto FrontEnd (click derecho al proyecto, luego debug/new instance). O usando el comando dotnet run:
  - Para el backend: dotnet run V8.Api.csproj, desde la capa de presentación.
  - Para el frontend: dotnet run V8.Web.csproj, desde la capa de presentación.