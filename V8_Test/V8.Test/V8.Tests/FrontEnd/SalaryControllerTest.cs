﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using V8.Web.Controllers;
using V8.Web.Exceptions;
using V8.Web.ViewModel;
using Xunit;

namespace V8.Tests.FrontEnd
{
    public class SalaryControllerTest : IDisposable
    {
        private SalaryController _salaryController;
        private Mock<IHttpClientFactory> _httpClientFactoryMock;
        private Mock<HttpMessageHandler> _httpMessageHandlerMock;
        private Mock<ISession> _sessionMock;
        private Mock<HttpContext> _httpContextMock;
        public SalaryControllerTest()
        {
            _httpClientFactoryMock = new Mock<IHttpClientFactory>();
            _httpMessageHandlerMock = new Mock<HttpMessageHandler>();
            _httpContextMock = new Mock<HttpContext>();
            _sessionMock = new Mock<ISession>();

            var sessionObject = DataSource.GetSalaryDetail();
            var sessionValue = JsonConvert.SerializeObject(sessionObject);
            byte[] dummyValue = Encoding.UTF8.GetBytes(sessionValue);

            _sessionMock.Setup(x => x.TryGetValue(It.IsAny<string>(), out dummyValue)).Returns(true);
            _httpContextMock.Setup(x => x.Session).Returns(_sessionMock.Object);
        }
        public void Dispose()
        {
            _salaryController.Dispose();
        }

        [Fact(DisplayName = "Add salary for the same year and month should return Conflict(409) status"), Trait("Category", "Unit")]
        public async Task AddSalaryForTheSameYearAndMonth_Fail()
        {
            //Arrange
            var newSalary = new SalaryViewModel
            {
                Year = 2020,
                Month = 5,
                IdOffice = 1,
                EmployeeCode = "11111111",
                EmployeeName = "EDUARDO",
                EmployeeSurname = "DIAZ",
                IdDivision = 1,
                IdPosition = 1,
                Grade = 5,
                BeginDate = DateTime.Parse("2018-01-01"),
                Birthday = DateTime.Parse("1984-11-01"),
                IdentificationNumber = "42753575",
                BaseSalary = 3000.00M,
                ProductionBonus = 0.00M,
                CompensationBonus = 0.00M,
                Commission = 0.00M,
                Contributions = 0.00M
            };

            SetOfficeAndPositionAndDivisionEndpointsConfiguration(newSalary);

            var client = new HttpClient(_httpMessageHandlerMock.Object);
            _httpClientFactoryMock.Setup(x => x.CreateClient(It.IsAny<string>())).Returns(client);

            _salaryController = new SalaryController(_httpClientFactoryMock.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = _httpContextMock.Object
                }
            };

            //Act
            var result = await _salaryController.AddSalary(newSalary);

            //Assert
            var conflictResult = Assert.IsType<ConflictObjectResult>(result);
            Assert.Equal(409, conflictResult.StatusCode);
            Assert.Equal($"Salary for EmployeeCode {newSalary.EmployeeCode} belonging to period: year {newSalary.Year} and month {newSalary.Month} already added to detail salarys.", conflictResult.Value);
        }

        [Fact(DisplayName = "Add salary with different name or surname for the same employee code should return Conflict(409) status"), Trait("Category", "Unit")]
        public async Task AddSalaryWithDifferentNameOrSurnameForTheSameEmployeeCode_Fail()
        {
            //Arrange
            var newSalary = new SalaryViewModel
            {
                Year = 2020,
                Month = 6,
                IdOffice = 1,
                EmployeeCode = "11111111",
                EmployeeName = "EDWARD",
                EmployeeSurname = "DIAZ",
                IdDivision = 1,
                IdPosition = 1,
                Grade = 5,
                BeginDate = DateTime.Parse("2018-01-01"),
                Birthday = DateTime.Parse("1984-11-01"),
                IdentificationNumber = "42753575",
                BaseSalary = 3000.00M,
                ProductionBonus = 0.00M,
                CompensationBonus = 0.00M,
                Commission = 0.00M,
                Contributions = 0.00M
            };

            SetOfficeAndPositionAndDivisionEndpointsConfiguration(newSalary);

            var client = new HttpClient(_httpMessageHandlerMock.Object);
            _httpClientFactoryMock.Setup(x => x.CreateClient(It.IsAny<string>())).Returns(client);

            _salaryController = new SalaryController(_httpClientFactoryMock.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = _httpContextMock.Object
                }
            };

            //Act
            var result = await _salaryController.AddSalary(newSalary);

            //Assert
            var conflictResult = Assert.IsType<ConflictObjectResult>(result);
            Assert.Equal(409, conflictResult.StatusCode);
            Assert.Equal($"Employee Full Name {newSalary.EmployeeName + " " + newSalary.EmployeeSurname} is different than the one already added to the salary detail.", conflictResult.Value);
        }

        [Fact(DisplayName = "Add salary for the same year and different month should be added to detail"), Trait("Category", "Unit")]
        public async Task AddSalaryForTheSameYearWithDifferentMonthShouldBeAddedToDetail_Success()
        {
            //Arrange
            var newSalary = new SalaryViewModel
            {
                Year = 2020,
                Month = 6,
                IdOffice = 1,
                EmployeeCode = "11111111",
                EmployeeName = "EDUARDO",
                EmployeeSurname = "DIAZ",
                IdDivision = 1,
                IdPosition = 1,
                Grade = 5,
                BeginDate = DateTime.Parse("2018-01-01"),
                Birthday = DateTime.Parse("1984-11-01"),
                IdentificationNumber = "42753575",
                BaseSalary = 3000.00M,
                ProductionBonus = 0.00M,
                CompensationBonus = 0.00M,
                Commission = 0.00M,
                Contributions = 0.00M
            };

            SetOfficeAndPositionAndDivisionEndpointsConfiguration(newSalary);

            var client = new HttpClient(_httpMessageHandlerMock.Object);
            _httpClientFactoryMock.Setup(x => x.CreateClient(It.IsAny<string>())).Returns(client);

            _salaryController = new SalaryController(_httpClientFactoryMock.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = _httpContextMock.Object
                }
            };

            //Act
            var result = await _salaryController.AddSalary(newSalary);

            //Assert
            Assert.True(newSalary.listOfSalarys.Count == 2);
        }

        private void SetOfficeAndPositionAndDivisionEndpointsConfiguration(SalaryViewModel newSalary)
        {
            _httpMessageHandlerMock.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.Is<HttpRequestMessage>(r => r.Method == HttpMethod.Get
                && r.RequestUri == new Uri($"{DataSource.GetListOfEnpoints(newSalary.IdOffice)["GetOfficeById"]}")), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(DataSource.GetOfficeById(newSalary.IdOffice)),
                });

            _httpMessageHandlerMock.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.Is<HttpRequestMessage>(r => r.Method == HttpMethod.Get
                && r.RequestUri == new Uri($"{DataSource.GetListOfEnpoints(newSalary.IdDivision)["GetDivisionById"]}")), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(DataSource.GetDivisionById(newSalary.IdDivision)),
                });

            _httpMessageHandlerMock.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.Is<HttpRequestMessage>(r => r.Method == HttpMethod.Get
                && r.RequestUri == new Uri($"{DataSource.GetListOfEnpoints(newSalary.IdPosition)["GetPositionById"]}")), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(DataSource.GetPositionById(newSalary.IdPosition)),
                });
        }
    }
}
