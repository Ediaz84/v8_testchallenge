﻿using System;
using System.Collections.Generic;
using V8.Web.ViewModel;

namespace V8.Tests.FrontEnd
{
    public static class DataSource
    {
        public static List<SalaryDetailViewModel> GetSalaryDetail()
        {
            var listOfSalarys = new List<SalaryDetailViewModel> {
                new SalaryDetailViewModel
                {
                    Year = 2020,
                    Month = 5,
                    IdOffice = 1,
                    EmployeeCode = "11111111",
                    EmployeeName = "EDUARDO",
                    EmployeeSurname = "DIAZ",
                    IdDivision = 1,
                    IdPosition = 1,
                    Grade = 5,
                    BeginDate = DateTime.Parse("2018-01-01"),
                    Birthday = DateTime.Parse("1984-11-01"),
                    IdentificationNumber = "42753575",
                    BaseSalary = 3000.00M,
                    ProductionBonus = 0.00M,
                    CompensationBonus = 0.00M,
                    Commission = 0.00M,
                    Contributions = 0.00M
                }
            };

            return listOfSalarys;
        }

        public static Dictionary<string, string> GetListOfEnpoints(int parameter)
        {
            var listOfEndpoints = new Dictionary<string, string>
            {
                { "GetOfficeById", "http://localhost:5142/api/Office/"+parameter },
                { "GetPositionById", "http://localhost:5142/api/Position/"+parameter },
                { "GetDivisionById", "http://localhost:5142/api/Division/"+parameter}
            };

            return listOfEndpoints;
        }

        private static Dictionary<int, string> GetListOfOffices()
        {
            return new Dictionary<int, string>
            {
                { 1, "{\"id\": 1,\"name\": \"A\"}" },
                { 2, "{\"id\": 2,\"name\": \"C\"}" },
                { 3, "{\"id\": 3,\"name\": \"D\"}" },
                { 4, "{\"id\": 4,\"name\": \"ZZ\"}" }
            };
        }

        private static Dictionary<int, string> GetListOfPositions()
        {
            return new Dictionary<int, string>
            {
                { 1, "{\"id\": 1,\"name\": \"CARGO MANAGER\"}" },
                { 2, "{\"id\": 2,\"name\": \"HEAD OF CARGO\"}" },
                { 3, "{\"id\": 3,\"name\": \"CARGO ASSISTANT\"}" },
                { 4, "{\"id\": 4,\"name\": \"SALES MANAGER\"}" },
                { 5, "{\"id\": 4,\"name\": \"ACCOUNT EXECUTIVE\"}" },
                { 6, "{\"id\": 4,\"name\": \"MARKETING ASSISTANT\"}" },
                { 7, "{\"id\": 4,\"name\": \"CUSTOMER DIRECTOR\"}" },
                { 8, "{\"id\": 4,\"name\": \"CUSTOMER ASSISTANT\"}" }
            };
        }

        private static Dictionary<int, string> GetListOfDivisions()
        {
            return new Dictionary<int, string>
            {
                { 1, "{\"id\": 1,\"name\": \"OPERATION\"}" },
                { 2, "{\"id\": 2,\"name\": \"SALES\"}" },
                { 3, "{\"id\": 3,\"name\": \"MARKETING\"}" },
                { 4, "{\"id\": 4,\"name\": \"CUSTOMER CARE\"}" }
            };
        }

        public static string GetOfficeById(int id)
        {
            var office = GetListOfOffices()[id];

            return office;
        }

        public static string GetPositionById(int id)
        {
            var position = GetListOfPositions()[id];

            return position;
        }

        public static string GetDivisionById(int id)
        {
            var division = GetListOfDivisions()[id];

            return division;
        }
    }
}
