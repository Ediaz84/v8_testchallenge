﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using V8.Domain.Core;
using V8.Domain.Interfaces.Repositories;
using V8.Domain.Interfaces.Services;
using Xunit;

namespace V8.Tests.BackEnd.Services
{
    public class OfficeServiceTest : IDisposable
    {
        private IOfficeService _officeService;
        private readonly Mock<IOfficeRepository> _officeRepositoryMock;
        public OfficeServiceTest()
        {
            _officeRepositoryMock = new Mock<IOfficeRepository>();
            _officeService = new OfficeService(_officeRepositoryMock.Object);
        }

        public void Dispose()
        {
            _officeService.Dispose();
        }

        [Fact(DisplayName = "Get All offices"), Trait("Category", "Unit")]
        public void GetAllOffices_Success()
        {
            //Arrange
            _officeRepositoryMock.Setup(x => x.GetAll()).Returns(DataSource.GetAllOffices());

            //Act
            var result = _officeService.GetAll();

            //Assert
            Assert.NotNull(result);
            Assert.True(result.Any());
        }

        [Fact(DisplayName = "Get office by id"), Trait("Category", "Unit")]
        public void GetOfficeById_Success()
        {
            var idOffice = 1;
            //Arrange
            _officeRepositoryMock.Setup(x => x.GetByid(idOffice)).Returns(DataSource.GetOfficeById(idOffice));

            //Act
            var result = _officeService.GetByid(idOffice);

            //Assert
            Assert.NotNull(result);
            Assert.True(result.Id > 0);
        }
    }
}
