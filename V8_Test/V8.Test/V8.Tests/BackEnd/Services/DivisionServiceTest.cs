﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using V8.Domain.Core;
using V8.Domain.Interfaces.Repositories;
using V8.Domain.Interfaces.Services;
using Xunit;

namespace V8.Tests.BackEnd.Services
{
    public class DivisionServiceTest : IDisposable
    {
        private IDivisionService _divisionService;
        private readonly Mock<IDivisionRepository> _divisionRepositoryMock;
        public DivisionServiceTest()
        {
            _divisionRepositoryMock = new Mock<IDivisionRepository>();
            _divisionService = new DivisionService(_divisionRepositoryMock.Object);
        }

        public void Dispose()
        {
            _divisionService.Dispose();
        }

        [Fact(DisplayName = "Get All Divisions"), Trait("Category", "Unit")]
        public void GetAllDivisions_Success()
        {
            //Arrange
            _divisionRepositoryMock.Setup(x => x.GetAll()).Returns(DataSource.GetAllDivisions());

            //Act
            var result = _divisionService.GetAll();

            //Assert
            Assert.NotNull(result);
            Assert.True(result.Any());
        }

        [Fact(DisplayName = "Get division by id"), Trait("Category", "Unit")]
        public void GetDivisionById_Success()
        {
            var divisionId = 1;

            //Arrange
            _divisionRepositoryMock.Setup(x => x.GetByid(divisionId)).Returns(DataSource.GetDivisionById(divisionId));

            //Act
            var result = _divisionService.GetByid(divisionId);

            //Assert
            Assert.NotNull(result);
            Assert.True(result.Id > 0);
        }
    }
}
