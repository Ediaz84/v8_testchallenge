﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using V8.Domain.Core;
using V8.Domain.Core.SalarySpecification;
using V8.Domain.Entities;
using V8.Domain.Interfaces.Repositories;
using V8.Domain.Interfaces.Services;
using V8.Domain.Interfaces.Specification;
using Xunit;

namespace V8.Tests.BackEnd.Services
{
    public class SalaryServiceTest : IDisposable
    {
        private ISalaryService _salaryService;
        private readonly Mock<ISalaryRepository> _salaryRepositoryMock;
        private ISpecification<Salary> _specification;
        private List<Salary> _postSalarys;
        public SalaryServiceTest()
        {
            _salaryRepositoryMock = new Mock<ISalaryRepository>();
            _salaryService = new SalaryService(_salaryRepositoryMock.Object);
            _specification = new SalaryWithOfficeDivisionAndPositionSpecification();
            GivenSalarys();
        }

        public void Dispose()
        {
            _salaryService.Dispose();
            _specification = null;
        }

        [Fact(DisplayName = "Given Salarys For Post")]
        public void GivenSalarys()
        {
            var salarys = new List<Salary>
            {
                new Salary
                {
                    Id = 5, Year = 2020, Month = 10, IdOffice = 4,
                    Office = new Office { Id = 4, Name = "ZZ" }, IdDivision = 2, IdPosition = 4,
                    EmployeeCode = "10023001", EmployeeName = "KALI", EmployeeSurname = "PRASAD",
                    Division = new Division { Id = 2, Name = "SALES" },
                    Position = new Position { Id = 4, Name = "SALES MANAGER" },
                    Grade = 18, BeginDate = DateTime.Parse("2020-09-09"), Birthday = DateTime.Parse("1980-01-25"),
                    IdentificationNumber = "14598756", BaseSalary = 2995.00M, ProductionBonus = 3000.00M,
                    CompensationBonus = 3200.00M, Commission = 0.00M, Contributions = 2100.00M
                },
                new Salary
                {
                    Id = 6, Year = 2020, Month = 9, IdOffice = 4,
                    Office = new Office { Id = 4, Name = "ZZ" }, IdDivision = 2, IdPosition = 4,
                    EmployeeCode = "10023001", EmployeeName = "KALI", EmployeeSurname = "PRASAD",
                    Division = new Division { Id = 2, Name = "SALES" },
                    Position = new Position { Id = 4, Name = "SALES MANAGER" },
                    Grade = 18, BeginDate = DateTime.Parse("2020-09-09"), Birthday = DateTime.Parse("1980-01-25"),
                    IdentificationNumber = "14598756", BaseSalary = 2995.00M, ProductionBonus = 3000.00M,
                    CompensationBonus = 3200.00M, Commission = 0.00M, Contributions = 2340.00M
                }
            };

            _postSalarys = salarys;
        }

        [Fact(DisplayName = "Get All Salarys With office, division and position"), Trait("Category", "Unit")]
        public void GetAllSalarysWithOfficeDivisionAndPosition_Success()
        {
            //Arrange
            _salaryRepositoryMock.Setup(x => x.FindWithSpecificationPattern(_specification)).Returns(DataSource.GetAllSalarys());

            //Act
            var result = _salaryService.FindWithSpecificationPattern(_specification);

            //Assert
            Assert.NotNull(result);
            Assert.True(result.Any());
        }

        [Theory(DisplayName = "Filters Salarys by office and division and position"), Trait("Category", "Unit")]
        [InlineData(3, 1, 2)]
        public void FilterSalarysByOfficeAndDivisionAndPosition(int idOffice, int idDivision, int idPosition)
        {
            //Arrange
            _salaryRepositoryMock.Setup(x => x.FilterLastSalarys(idOffice, idDivision, idPosition))
                .Returns(DataSource.FilterSalarysByOfficeAndDivisionAndPosition(idOffice, idDivision, idPosition));

            //Act
            var result = _salaryService.FilterLastSalarys(idOffice, idDivision, idPosition);

            //Assert
            Assert.NotNull(result);
            Assert.True(result.Any());
            Assert.True(result.Count() == 2);
        }

        [Fact(DisplayName = "Add Multiple Salarys"), Trait("Category", "Unit")]
        public void AddMultipleSalarys_Success()
        {
            //Given
            var salarys = _postSalarys;
            var added = DataSource.AddMultipleSalarys(salarys);

            //Arrange
            _salaryRepositoryMock.Setup(x => x.AddRange(It.IsAny<IEnumerable<Salary>>())).Returns(added);

            //Act
            var result = _salaryService.AddRange(salarys);

            //Assert
            Assert.True(result == 1);
        }

        [Fact(DisplayName = "Add Multiple Salarys"), Trait("Category", "Unit")]
        public void GetLastThreeConsecutiveSalaries_Success()
        {
            //Given
            var employeeCode = "10001222";
            var lastThreeConsecutiveSalarys = DataSource.GetLastThreeConsecutiveSalaries(employeeCode);

            //Arrange
            _salaryRepositoryMock.Setup(x => x.GetLastThreeSalarysWithBonoCalculated(employeeCode)).Returns(lastThreeConsecutiveSalarys);

            //Act
            var result = _salaryService.GetLastThreeSalarysWithBonoCalculated(employeeCode);

            //Assert
            Assert.True(result.Any());
        }
    }
}
