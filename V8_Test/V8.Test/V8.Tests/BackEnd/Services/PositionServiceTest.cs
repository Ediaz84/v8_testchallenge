﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using V8.Domain.Core;
using V8.Domain.Interfaces.Repositories;
using V8.Domain.Interfaces.Services;
using Xunit;

namespace V8.Tests.BackEnd.Services
{
    public class PositionServiceTest : IDisposable
    {
        private IPositionService _positionService;
        private readonly Mock<IPositionRepository> _positionRepositoryMock;
        public PositionServiceTest()
        {
            _positionRepositoryMock = new Mock<IPositionRepository>();
            _positionService = new PositionService(_positionRepositoryMock.Object);
        }

        public void Dispose()
        {
            _positionService.Dispose();
        }

        [Fact(DisplayName = "Get All positions"), Trait("Category", "Unit")]
        public void GetAllPositions_Success()
        {
            //Arrange
            _positionRepositoryMock.Setup(x => x.GetAll()).Returns(DataSource.GetAllPositions());

            //Act
            var result = _positionService.GetAll();

            //Assert
            Assert.NotNull(result);
            Assert.True(result.Any());
        }

        [Fact(DisplayName = "Get position by id"), Trait("Category", "Unit")]
        public void GetPositionById_Success()
        {
            var positionId = 1;

            //Arrange
            _positionRepositoryMock.Setup(x => x.GetByid(positionId)).Returns(DataSource.GetPositionById(positionId));

            //Act
            var result = _positionService.GetByid(positionId);

            //Assert
            Assert.NotNull(result);
            Assert.True(result.Id > 0);
        }
    }
}
