﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using V8.Domain.Entities;

namespace V8.Tests.BackEnd
{
    public static class DataSource
    {
        public static IEnumerable<Salary> GetAllSalarys()
        {
            var listOfSalarys = new List<Salary> {
                new Salary
                {
                    Id = 1, Year = 2020, Month = 10, IdOffice = 2,
                    Office = new Office { Id = 2, Name = "C" }, IdDivision = 2, IdPosition = 1,
                    EmployeeCode = "10001222", EmployeeName = "MIKE", EmployeeSurname = "JAMES",
                    Division = new Division { Id = 1, Name = "OPERATION" },
                    Position = new Position { Id = 1, Name = "CARGO MANAGER" },
                    Grade = 18, BeginDate = DateTime.Parse("2020-05-11"), Birthday = DateTime.Parse("1990-03-15"),
                    IdentificationNumber = "45642132", BaseSalary = 2799.45M, ProductionBonus = 0.00M,
                    CompensationBonus = 3215.63M, Commission = 0.00M, Contributions = 1000.00M
                },
                new Salary
                {
                    Id = 2, Year = 2020, Month = 9, IdOffice = 2,
                    Office = new Office { Id = 2, Name = "C" }, IdDivision = 1, IdPosition = 1,
                    EmployeeCode = "10001222", EmployeeName = "MIKE", EmployeeSurname = "JAMES",
                    Division = new Division { Id = 1, Name = "OPERATION" },
                    Position = new Position { Id = 1, Name = "CARGO MANAGER" },
                    Grade = 18, BeginDate = DateTime.Parse("2020-05-11"), Birthday = DateTime.Parse("1990-03-15"),
                    IdentificationNumber = "45642132", BaseSalary = 2799.45M, ProductionBonus = 0.00M,
                    CompensationBonus = 3263.99M, Commission = 0.00M, Contributions = 1000.00M
                },
                new Salary
                {
                    Id = 3, Year = 2020, Month = 8, IdOffice = 3,
                    Office = new Office { Id = 3, Name = "D" }, IdDivision = 1, IdPosition = 2,
                    EmployeeCode = "10001222", EmployeeName = "MIKE", EmployeeSurname = "JAMES",
                    Division = new Division { Id = 1, Name = "OPERATION" },
                    Position = new Position { Id = 2, Name = "HEAD OF CARGO" },
                    Grade = 12, BeginDate = DateTime.Parse("2020-05-11"), Birthday = DateTime.Parse("1990-03-15"),
                    IdentificationNumber = "45642132", BaseSalary = 2799.45M, ProductionBonus = 0.00M,
                    CompensationBonus = 998.45M, Commission = 0.00M, Contributions = 600.00M
                },
                new Salary
                {
                    Id = 4, Year = 2020, Month = 7, IdOffice = 3,
                    Office = new Office { Id = 3, Name = "D" }, IdDivision = 1, IdPosition = 2,
                    EmployeeCode = "10001222", EmployeeName = "MIKE", EmployeeSurname = "JAMES",
                    Division = new Division { Id = 1, Name = "OPERATION" },
                    Position = new Position { Id = 2, Name = "HEAD OF CARGO" },
                    Grade = 12, BeginDate = DateTime.Parse("2020-05-11"), Birthday = DateTime.Parse("1990-03-15"),
                    IdentificationNumber = "45642132", BaseSalary = 2799.45M, ProductionBonus = 0.00M,
                    CompensationBonus = 998.45M, Commission = 0.00M, Contributions = 600.00M
                }
            };

            return listOfSalarys;
        }

        public static IEnumerable<Salary> FilterSalarysByOfficeAndDivisionAndPosition(int idOffice, int idDivision, int idPosition)
        {
            var salarys = GetAllSalarys().Where(x => x.IdOffice == idOffice && x.IdDivision == idDivision && x.IdPosition == idPosition);

            return salarys.ToList();
        }

        public static int AddMultipleSalarys(IEnumerable<Salary> salaries)
        {
            var actualSalarys = GetAllSalarys().ToList();
            actualSalarys.AddRange(salaries);

            return actualSalarys.ToList().Count == 6 ? 1 : 0;
        }

        public static IEnumerable<Salary> GetLastThreeConsecutiveSalaries(string employeeCode)
        {
            var salarys = GetAllSalarys().Where(x => x.EmployeeCode == employeeCode)
                .OrderByDescending(x => x.Year)
                .ThenByDescending(x => x.Month)
                .Take(3)
                .ToList();

            IList<Salary> resultList = new List<Salary>();

            for (int i = 0; i < salarys.Count; i++)
            {
                if (salarys[0].Month - i == salarys[i].Month)
                {
                    resultList.Add(salarys[i]);
                }
            }
            return resultList;
        }

        public static IEnumerable<Office> GetAllOffices()
        {
            var listOfOffices = new List<Office>
            {
                new Office { Id = 1, Name = "A" },
                new Office { Id = 2, Name = "C" },
                new Office { Id = 3, Name = "D" },
                new Office { Id = 4, Name = "ZZ"}
            };

            return listOfOffices;
        }
        
        public static IEnumerable<Position> GetAllPositions()
        {
            var listOfPositions = new List<Position>
            {
                new Position { Id = 1, Name = "CARGO MANAGER" },
                new Position { Id = 2, Name = "HEAD OF CARGO" },
                new Position { Id = 3, Name = "CARGO ASSISTANT" },
                new Position { Id = 4, Name = "SALES MANAGER" },
                new Position { Id = 5, Name = "ACCOUNT EXECUTIVE" },
                new Position { Id = 6, Name = "MARKETING ASSISTANT" },
                new Position { Id = 7, Name = "CUSTOMER DIRECTOR" },
                new Position { Id = 8, Name = "CUSTOMER ASSISTANT" }
            };

            return listOfPositions;
        }

        public static IEnumerable<Division> GetAllDivisions()
        {
            var listOfDivisions = new List<Division>
            {
                new Division { Id = 1, Name = "OPERATION" },
                new Division { Id = 2, Name = "SALES" },
                new Division { Id = 3, Name = "MARKETING" },
                new Division { Id = 4, Name = "CUSTOMER CARE" }
            };

            return listOfDivisions;
        }

        public static Office GetOfficeById(int id)
        {
            var office = GetAllOffices().Where(x => x.Id == id).FirstOrDefault();

            return office;
        }

        public static Position GetPositionById(int id)
        {
            var position = GetAllPositions().Where(x => x.Id == id).FirstOrDefault();

            return position;
        }

        public static Division GetDivisionById(int id)
        {
            var division = GetAllDivisions().Where(x => x.Id == id).FirstOrDefault();

            return division;
        }
    }
}
