﻿using System;
using System.Collections.Generic;
using System.Text;
using V8.Application.Main.Interfaces;
using V8.Domain.Entities;
using V8.Domain.Interfaces.Services;

namespace V8.Application.Main.Services
{
    public class PositionAppService : BaseAppService<Position>, IPositionAppService
    {
        private readonly IPositionService _positionService;

        public PositionAppService(IPositionService positionService) : base(positionService)
        {
            _positionService = positionService;
        }
    }
}