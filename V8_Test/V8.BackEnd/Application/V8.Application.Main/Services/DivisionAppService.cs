﻿using System;
using System.Collections.Generic;
using System.Text;
using V8.Application.Main.Interfaces;
using V8.Domain.Entities;
using V8.Domain.Interfaces.Services;

namespace V8.Application.Main.Services
{
    public class DivisionAppService : BaseAppService<Division>, IDivisionAppService
    {
        private readonly IDivisionService _divisionService;

        public DivisionAppService(IDivisionService divisionService) : base(divisionService)
        {
            _divisionService = divisionService;
        }
    }
}