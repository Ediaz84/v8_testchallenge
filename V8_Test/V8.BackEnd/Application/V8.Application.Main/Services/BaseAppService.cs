﻿using System;
using System.Collections.Generic;
using System.Text;
using V8.Application.Main.Interfaces;
using V8.Domain.Interfaces.Services;
using V8.Domain.Interfaces.Specification;

namespace V8.Application.Main.Services
{
    public class BaseAppService<TEntity> : IDisposable, IBaseAppService<TEntity> where TEntity : class
    {
        private readonly IBaseService<TEntity> _baseService;

        public BaseAppService(IBaseService<TEntity> baseService)
        {
            _baseService = baseService;
        }

        public int Add(TEntity entity)
        {
            return _baseService.Add(entity);
        }

        public int AddRange(IEnumerable<TEntity> list)
        {
            return _baseService.AddRange(list);
        }

        public void Delete(int id)
        {
            _baseService.Delete(id);
        }

        public void Dispose()
        {
            _baseService.Dispose();
        }

        public IEnumerable<TEntity> FindWithSpecificationPattern(ISpecification<TEntity> specification = null)
        {
            return _baseService.FindWithSpecificationPattern(specification);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _baseService.GetAll();
        }

        public TEntity GetByid(int id)
        {
            return _baseService.GetByid(id);
        }

        public bool Update(TEntity entity)
        {
            return _baseService.Update(entity);
        }
    }
}
