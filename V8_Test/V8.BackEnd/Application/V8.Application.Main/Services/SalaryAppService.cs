﻿using System;
using System.Collections.Generic;
using System.Text;
using V8.Application.Main.Interfaces;
using V8.Domain.Entities;
using V8.Domain.Interfaces.Services;

namespace V8.Application.Main.Services
{
    public class SalaryAppService : BaseAppService<Salary>, ISalaryAppService
    {
        private readonly ISalaryService _salaryService;

        public SalaryAppService(ISalaryService salaryService) : base(salaryService)
        {
            _salaryService = salaryService;
        }

        public IEnumerable<Salary> FilterLastSalarys(int IdOffice, int IdDivision, int IdPosition)
        {
            return _salaryService.FilterLastSalarys(IdOffice, IdDivision, IdPosition);
        }

        public IEnumerable<Salary> GetLastSalarys()
        {
            return _salaryService.GetLastSalarys();
        }

        public IEnumerable<Salary> GetLastThreeSalarysWithBonoCalculated(string employeeCode)
        {
            return _salaryService.GetLastThreeSalarysWithBonoCalculated(employeeCode);
        }
    }
}
