﻿using System;
using System.Collections.Generic;
using System.Text;
using V8.Application.Main.Interfaces;
using V8.Domain.Entities;
using V8.Domain.Interfaces.Services;

namespace V8.Application.Main.Services
{
    public class OfficeAppService : BaseAppService<Office>, IOfficeAppService
    {
        private readonly IOfficeService _officeService;

        public OfficeAppService(IOfficeService officeService) : base(officeService)
        {
            _officeService = officeService;
        }
    }
}
