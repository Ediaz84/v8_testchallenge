﻿using System;
using System.Collections.Generic;
using System.Text;
using V8.Domain.Entities;

namespace V8.Application.Main.Interfaces
{
    public interface IPositionAppService : IBaseAppService<Position>
    {
    }
}
