﻿using System;
using System.Collections.Generic;
using System.Text;
using V8.Domain.Entities;

namespace V8.Application.Main.Interfaces
{
    public interface ISalaryAppService : IBaseAppService<Salary>
    {
        IEnumerable<Salary> GetLastSalarys();
        IEnumerable<Salary> FilterLastSalarys(int IdOffice, int IdDivision, int IdPosition);
        IEnumerable<Salary> GetLastThreeSalarysWithBonoCalculated(string employeeCode);
    }
}
