﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using V8.Api.Dto;
using V8.Application.Main.Interfaces;
using V8.Domain.Core.SalarySpecification;
using V8.Domain.Entities;

namespace V8.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SalaryController : ControllerBase
    {
        private readonly ILogger<SalaryController> _logger;
        private readonly IMapper _mapper;
        private readonly ISalaryAppService _salaryAppService;

        public SalaryController(ILogger<SalaryController> logger, IMapper mapper, ISalaryAppService salaryAppService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _salaryAppService = salaryAppService ?? throw new ArgumentNullException(nameof(salaryAppService));            
        }

        /// <summary>
        /// Get all salarys.
        /// </summary>
        /// <remarks>
        /// Get all employees salarys
        /// </remarks>
        /// <returns>
        /// Returns status request 200 ok.
        /// </returns>
        [HttpGet]
        public IActionResult GetAll()
        {
            var specification = new SalaryWithOfficeDivisionAndPositionSpecification();
            var salarys = _salaryAppService.FindWithSpecificationPattern(specification);
            var returnList = _mapper.Map<IEnumerable<Salary>, IEnumerable<GetSalaryDto>>(salarys);

            return Ok(returnList);
        }

        /// <summary>
        /// Get all calculated salarys.
        /// </summary>
        /// <remarks>
        /// Get all calculated salarys by this formula
        /// Other Income = (Base Salary + Commission) * 8% + Commission 
        /// Total Salary = Base Salary + Production Bonus + (Compensation Bonus * 75%) + Other Income - Contributions
        /// </remarks>
        /// <returns>
        /// Returns status request 200 ok.
        /// </returns>
        [HttpGet("Calculated")]
        public IActionResult GetAllCalculated()
        {
            var salarys = _salaryAppService.GetLastSalarys();
            var returnList = _mapper.Map<IEnumerable<Salary>, IEnumerable<GetTotalSalaryDto>>(salarys);

            return Ok(returnList);
        }

        /// <summary>
        /// Get all filtered calculated salarys.
        /// </summary>
        /// <remarks>
        /// Get all filtered calculated salarys by this formula
        /// Other Income = (Base Salary + Commission) * 8% + Commission 
        /// Total Salary = Base Salary + Production Bonus + (Compensation Bonus * 75%) + Other Income - Contributions
        /// </remarks>
        /// <param name="filter">Filter Salary Dto</param>
        /// <returns>
        /// Returns status request 200 ok.
        /// </returns>
        [HttpPost("FilterCalculated")]
        public IActionResult GetFilterCalculated(FilterSalaryDto filter)
        {
            var salarys = _salaryAppService.FilterLastSalarys(filter.IdOffice, filter.IdDivision, filter.IdPosition);
            var returnList = _mapper.Map<IEnumerable<Salary>, IEnumerable<GetTotalSalaryDto>>(salarys);

            return Ok(returnList);
        }

        /// <summary>
        /// Post salarys.
        /// </summary>
        /// <remarks>
        /// Post a list of salarys
        /// </remarks>
        /// <returns>
        /// Returns status request 200 ok.
        /// </returns>
        [HttpPost]
        public IActionResult PostSalary([FromBody] IEnumerable<PostSalaryDto> list)
        {
            var listOfSalarys = _mapper.Map<IEnumerable<PostSalaryDto>, IEnumerable<Salary>>(list);
            var returnValue = _salaryAppService.AddRange(listOfSalarys);

            return Ok(returnValue);
        }

        /// <summary>
        /// Get last 3 salarys to calculate bono.
        /// </summary>
        /// <remarks>
        /// Get last 3 salarys to calculate bono
        /// </remarks>
        /// <returns>
        /// Returns status request 200 ok.
        /// </returns>
        [HttpPost("Bono")]
        public IActionResult GetLasThreeSalarysToCalculateBono(FilterSalaryBonoDto filterSalaryBonoDto)
        {
            var salarys = _salaryAppService.GetLastThreeSalarysWithBonoCalculated(filterSalaryBonoDto.EmployeeCode);
            var returnList = _mapper.Map<IEnumerable<Salary>, IEnumerable<GetTotalSalaryDto>>(salarys);

            return Ok(returnList);
        }
    }
}
