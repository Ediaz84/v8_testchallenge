﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using V8.Api.Dto;
using V8.Application.Main.Interfaces;
using V8.Domain.Entities;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace V8.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PositionController : ControllerBase
    {
        private readonly ILogger<SalaryController> _logger;
        private readonly IMapper _mapper;
        private readonly IPositionAppService _positionAppService;

        public PositionController(ILogger<SalaryController> logger, IMapper mapper, IPositionAppService positionAppService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _positionAppService = positionAppService ?? throw new ArgumentNullException(nameof(positionAppService));
        }

        /// <summary>
        /// Get all divisions.
        /// </summary>
        /// <remarks>
        /// Get all divisions
        /// </remarks>
        /// <returns>
        /// Returns status request 200 ok.
        /// </returns>
        [HttpGet("All/{allOptionRequired}")]
        public IActionResult GetAll(bool allOptionRequired)
        {
            IEnumerable<GetPositionDto> returnList;
            var positions = _positionAppService.GetAll();

            if (allOptionRequired)
            {
                returnList = new List<GetPositionDto>()
                {
                    new GetPositionDto
                    {
                        Id = -1,
                        Name = "All"
                    }
                };
                var list = _mapper.Map<IEnumerable<Position>, IEnumerable<GetPositionDto>>(positions);
                var newList = returnList.Concat(list).ToList();

                return Ok(newList);
            }
            else
            {
                returnList = _mapper.Map<IEnumerable<Position>, IEnumerable<GetPositionDto>>(positions);

                return Ok(returnList);
            }
        }

        /// <summary>
        /// Get position by Id.
        /// </summary>
        /// <remarks>
        /// Get position by Id
        /// </remarks>
        /// <returns>
        /// Returns status request 200 ok.
        /// </returns>
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var position = _positionAppService.GetByid(id);
            var returnObject = _mapper.Map<Position, GetPositionDto>(position);

            return Ok(returnObject);
        }
    }
}
