﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using V8.Api.Dto;
using V8.Application.Main.Interfaces;
using V8.Domain.Entities;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace V8.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DivisionController : ControllerBase
    {
        private readonly ILogger<SalaryController> _logger;
        private readonly IMapper _mapper;
        private readonly IDivisionAppService _divisionAppService;

        public DivisionController(ILogger<SalaryController> logger, IMapper mapper, IDivisionAppService divisionAppService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _divisionAppService = divisionAppService ?? throw new ArgumentNullException(nameof(divisionAppService));
        }

        /// <summary>
        /// Get all divisions.
        /// </summary>
        /// <remarks>
        /// Get all divisions
        /// </remarks>
        /// <returns>
        /// Returns status request 200 ok.
        /// </returns>
        [HttpGet("All/{allOptionRequired}")]
        public IActionResult GetAll(bool allOptionRequired)
        { 
            IEnumerable<GetDivisionDto> returnList;
            var divisions = _divisionAppService.GetAll();

            if (allOptionRequired)
            {
                returnList = new List<GetDivisionDto>()
                {
                    new GetDivisionDto
                    {
                        Id = -1,
                        Name = "All"
                    }
                };
                var list = _mapper.Map<IEnumerable<Division>, IEnumerable<GetDivisionDto>>(divisions);
                var newList = returnList.Concat(list).ToList();

                return Ok(newList);
            }
            else
            {
                returnList = _mapper.Map<IEnumerable<Division>, IEnumerable<GetDivisionDto>>(divisions);

                return Ok(returnList);
            }
        }

        /// <summary>
        /// Get division by Id.
        /// </summary>
        /// <remarks>
        /// Get division by Id
        /// </remarks>
        /// <returns>
        /// Returns status request 200 ok.
        /// </returns>
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var division = _divisionAppService.GetByid(id);
            var returnObject = _mapper.Map<Division, GetDivisionDto>(division);

            return Ok(returnObject);
        }
    }
}
