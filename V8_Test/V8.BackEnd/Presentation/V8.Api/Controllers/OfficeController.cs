﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using V8.Api.Dto;
using V8.Application.Main.Interfaces;
using V8.Domain.Entities;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace V8.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OfficeController : ControllerBase
    {
        private readonly ILogger<SalaryController> _logger;
        private readonly IMapper _mapper;
        private readonly IOfficeAppService _officeAppService;

        public OfficeController(ILogger<SalaryController> logger, IMapper mapper, IOfficeAppService officeAppService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _officeAppService = officeAppService ?? throw new ArgumentNullException(nameof(officeAppService));
        }

        /// <summary>
        /// Get all offices.
        /// </summary>
        /// <remarks>
        /// Get all offices
        /// </remarks>
        /// <returns>
        /// Returns status request 200 ok.
        /// </returns>
        [HttpGet("All/{allOptionRequired}")]
        public IActionResult GetAll(bool allOptionRequired)
        {
            IEnumerable<GetOfficeDto> returnList;
            var offices = _officeAppService.GetAll();

            if (allOptionRequired)
            {
                returnList = new List<GetOfficeDto>()
                {
                    new GetOfficeDto
                    {
                        Id = -1,
                        Name = "All"
                    }
                };
                var list = _mapper.Map<IEnumerable<Office>, IEnumerable<GetOfficeDto>>(offices);
                var newList = returnList.Concat(list).ToList();

                return Ok(newList);
            }
            else
            {
                returnList = _mapper.Map<IEnumerable<Office>, IEnumerable<GetOfficeDto>>(offices);

                return Ok(returnList);
            }
        }

        /// <summary>
        /// Get office by Id.
        /// </summary>
        /// <remarks>
        /// Get office by Id
        /// </remarks>
        /// <returns>
        /// Returns status request 200 ok.
        /// </returns>
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var office = _officeAppService.GetByid(id);
            var returnObject = _mapper.Map<Office, GetOfficeDto>(office);

            return Ok(returnObject);
        }
    }
}
