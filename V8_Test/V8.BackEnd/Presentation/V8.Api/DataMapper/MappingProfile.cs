﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using V8.Api.Dto;
using V8.Domain.Entities;

namespace V8.Api.DataMapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
			CreateMap<Salary, GetSalaryDto>()
				.ForMember(opt => opt.Id, dest => dest.MapFrom(x => x.Id))
				.ForMember(opt => opt.Year, dest => dest.MapFrom(x => x.Year))
				.ForMember(opt => opt.Month, dest => dest.MapFrom(x => x.Month))
				.ForMember(opt => opt.EmployeeCode, dest => dest.MapFrom(x => x.EmployeeCode))
				.ForMember(opt => opt.EmployeeName, dest => dest.MapFrom(x => x.EmployeeName))
				.ForMember(opt => opt.EmployeeSurname, dest => dest.MapFrom(x => x.EmployeeSurname))
				.ForMember(opt => opt.Grade, dest => dest.MapFrom(x => x.Grade))
				.ForMember(opt => opt.BeginDate, dest => dest.MapFrom(x => x.BeginDate))
				.ForMember(opt => opt.Birthday, dest => dest.MapFrom(x => x.Birthday))
				.ForMember(opt => opt.IdentificationNumber, dest => dest.MapFrom(x => x.IdentificationNumber))
				.ForMember(opt => opt.BaseSalary, dest => dest.MapFrom(x => x.BaseSalary))
				.ForMember(opt => opt.ProductionBonus, dest => dest.MapFrom(x => x.ProductionBonus))
				.ForMember(opt => opt.CompensationBonus, dest => dest.MapFrom(x => x.CompensationBonus))
				.ForMember(opt => opt.Commission, dest => dest.MapFrom(x => x.Commission))
				.ForMember(opt => opt.Contributions, dest => dest.MapFrom(x => x.Contributions))
				.ForMember(opt => opt.Division, dest => dest.MapFrom(x => x.Division.Name))
				.ForMember(opt => opt.Office, dest => dest.MapFrom(x => x.Office.Name))
				.ForMember(opt => opt.Position, dest => dest.MapFrom(x => x.Position.Name));

			CreateMap<PostSalaryDto, Salary>();

			CreateMap<Office, GetOfficeDto>()
				.ForMember(opt => opt.Id, dest => dest.MapFrom(x => x.Id))
				.ForMember(opt => opt.Name, dest => dest.MapFrom(x => x.Name));

			CreateMap<Division, GetDivisionDto>()
				.ForMember(opt => opt.Id, dest => dest.MapFrom(x => x.Id))
				.ForMember(opt => opt.Name, dest => dest.MapFrom(x => x.Name));

			CreateMap<Position, GetPositionDto>()
				.ForMember(opt => opt.Id, dest => dest.MapFrom(x => x.Id))
				.ForMember(opt => opt.Name, dest => dest.MapFrom(x => x.Name));

			CreateMap<Salary, GetTotalSalaryDto>()
				.ForMember(opt => opt.EmployeeCode, dest => dest.MapFrom(x => x.EmployeeCode))
				.ForMember(opt => opt.EmployeeFullName, dest => dest.MapFrom(x => x.GetEmployeeFullName()))
				.ForMember(opt => opt.Office, dest => dest.MapFrom(x => x.Office.Name))
				.ForMember(opt => opt.Division, dest => dest.MapFrom(x => x.Division.Name))
				.ForMember(opt => opt.Position, dest => dest.MapFrom(x => x.Position.Name))
				.ForMember(opt => opt.BeginDate, dest => dest.MapFrom(x => x.BeginDate))
				.ForMember(opt => opt.Birthday, dest => dest.MapFrom(x => x.Birthday))
				.ForMember(opt => opt.IdentificationNumber, dest => dest.MapFrom(x => x.IdentificationNumber))
				.ForMember(opt => opt.TotalSalary, dest => dest.MapFrom(x => x.CalculateTotalSalary()));
		}
    }
}
