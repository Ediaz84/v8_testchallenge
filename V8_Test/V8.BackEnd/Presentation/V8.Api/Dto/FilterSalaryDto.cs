﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace V8.Api.Dto
{
    public class FilterSalaryDto
    {
        public int IdOffice { get; set; }
        public int IdPosition { get; set; }
        public int IdDivision { get; set; }
    }
}
