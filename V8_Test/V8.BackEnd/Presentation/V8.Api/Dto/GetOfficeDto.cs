﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace V8.Api.Dto
{
    public class GetOfficeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
