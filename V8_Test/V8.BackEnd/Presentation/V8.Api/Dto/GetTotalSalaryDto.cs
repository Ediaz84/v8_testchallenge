﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace V8.Api.Dto
{
    public class GetTotalSalaryDto
    {
		public string EmployeeCode { get; set; }
		public string EmployeeFullName { get; set; }
		public string Division { get; set; }
		public string Position { get; set; }
		public string Office { get; set; }
		public DateTime BeginDate { get; set; }
		public DateTime Birthday { get; set; }
		public string IdentificationNumber { get; set; }
		public decimal TotalSalary { get; set; }
	}
}
