using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using V8.Api.Common;
using V8.Application.Main.Interfaces;
using V8.Application.Main.Services;
using V8.Domain.Core;
using V8.Domain.Interfaces.Repositories;
using V8.Domain.Interfaces.Services;
using V8.Infraestructure.Data.Context;
using V8.Infraestructure.Data.Repositories;

namespace V8.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers()
                .AddFluentValidation(s =>
                {
                    s.RegisterValidatorsFromAssemblyContaining<Startup>();
                    s.RunDefaultMvcValidationAfterFluentValidationExecutes = false;
                });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "V8.Api", Version = "v1" });
                c.OperationFilter<SwaggerDefaultValues>();
                c.IncludeXmlComments(XmlCommentsFilePath);
            });

            //database context
            services.AddDbContext<ApplicationDbContext>(options =>
                options
                    .UseSqlServer(Configuration["AppSettings:ConnectionString"],
                    options =>
                    {
                        options.EnableRetryOnFailure();
                        options.CommandTimeout(int.MaxValue);
                    })
            );

            //automapper
            services.AddAutoMapper(typeof(Startup));

            //services application layer
            services.AddScoped(typeof(IBaseAppService<>), typeof(BaseAppService<>));
            services.AddScoped<ISalaryAppService, SalaryAppService>();
            services.AddScoped<IOfficeAppService, OfficeAppService>();
            services.AddScoped<IDivisionAppService, DivisionAppService>();
            services.AddScoped<IPositionAppService, PositionAppService>();
            //services application layer with domain services
            services.AddScoped(typeof(IBaseService<>), typeof(BaseService<>));
            services.AddScoped<ISalaryService, SalaryService>();
            services.AddScoped<IOfficeService, OfficeService>();
            services.AddScoped<IDivisionService, DivisionService>();
            services.AddScoped<IPositionService, PositionService>();
            //domain repositories to infraestructure layer
            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            services.AddScoped<ISalaryRepository, SalaryRepository>();
            services.AddScoped<IOfficeRepository, OfficeRepository>();
            services.AddScoped<IDivisionRepository, DivisionRepository>();
            services.AddScoped<IPositionRepository, PositionRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "V8.Api v1"));
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        string XmlCommentsFilePath
        {
            get
            {
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var fileName = typeof(Startup).GetTypeInfo().Assembly.GetName().Name + ".xml";
                return Path.Combine(basePath, fileName);
            }
        }
    }
}
