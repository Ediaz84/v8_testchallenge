﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Linq;

namespace V8.Domain.Entities
{
    [Table("Salary")]
    public class Salary : Entity<int>
    {
		public int Year { get; set; }
		public int Month { get; set; }
		public int IdOffice { get; set; }
		public string EmployeeCode { get; set; }
		public string EmployeeName { get; set; }
		public string EmployeeSurname { get; set; }
		public int IdDivision { get; set; }
		public int IdPosition { get; set; }
		public int Grade { get; set; }
		public DateTime BeginDate { get; set; }
		public DateTime Birthday { get; set; }
		public string IdentificationNumber { get; set; }
		public decimal BaseSalary { get; set; }
		public decimal ProductionBonus { get; set; }
		public decimal CompensationBonus { get; set; }
		public decimal Commission { get; set; }
		public decimal Contributions { get; set; }
		[ForeignKey("IdDivision")]
		public Division Division { get; set; }
		[ForeignKey("IdOffice")]
		public Office Office { get; set; }
		[ForeignKey("IdPosition")]
		public Position Position { get; set; }

		public decimal CalculateTotalSalary()
        {
			decimal otherIncome = (BaseSalary * Commission) * 0.08M + Commission;
			decimal totalSalary = Math.Round(BaseSalary + ProductionBonus + (CompensationBonus * 0.75M) + otherIncome - Contributions, 2, MidpointRounding.AwayFromZero);

			return totalSalary;
        }

		public string GetEmployeeFullName()
        {
			return EmployeeName + " " + EmployeeSurname;
        }
	}
}
