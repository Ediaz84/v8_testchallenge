﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using V8.Domain.Entities.SeedWork;

namespace V8.Domain.Entities
{
    public abstract class BaseEntity
    {
        
    }

    public abstract class Entity<T> : BaseEntity, IEntity<T>
    {
        [Key]
        public virtual T Id { get; set; }
    }
}
