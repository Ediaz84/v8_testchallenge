﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace V8.Domain.Entities
{
    [Table("Office")]
    public class Office : Entity<int>
    {
        public string Name { get; set; }
        public virtual ICollection<Salary> Salarys { get; set; }
    }
}
