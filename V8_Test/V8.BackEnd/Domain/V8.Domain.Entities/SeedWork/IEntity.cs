﻿using System;
using System.Collections.Generic;
using System.Text;

namespace V8.Domain.Entities.SeedWork
{
    public interface IEntity<T>
    {
        T Id { get; set; }
    }
}
