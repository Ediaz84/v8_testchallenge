﻿using System;
using System.Collections.Generic;
using System.Text;
using V8.Domain.Entities;

namespace V8.Domain.Interfaces.Repositories
{
    public interface IPositionRepository : IBaseRepository<Position>
    {
    }
}
