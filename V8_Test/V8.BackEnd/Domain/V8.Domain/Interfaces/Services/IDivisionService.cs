﻿using System;
using System.Collections.Generic;
using System.Text;
using V8.Domain.Entities;

namespace V8.Domain.Interfaces.Services
{
    public interface IDivisionService : IBaseService<Division>
    {
    }
}
