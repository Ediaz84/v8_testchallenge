﻿using System;
using System.Collections.Generic;
using System.Text;
using V8.Domain.Interfaces.Specification;

namespace V8.Domain.Interfaces.Services
{
    public interface IBaseService<TEntity> where TEntity : class
    {
        int Add(TEntity entity);
        int AddRange(IEnumerable<TEntity> list);
        void Delete(int id);
        bool Update(TEntity entity);
        IEnumerable<TEntity> GetAll();
        TEntity GetByid(int id);
        void Dispose();
        IEnumerable<TEntity> FindWithSpecificationPattern(ISpecification<TEntity> specification = null);
    }
}
