﻿using System;
using System.Collections.Generic;
using System.Text;
using V8.Domain.Interfaces.Repositories;
using V8.Domain.Interfaces.Services;
using V8.Domain.Interfaces.Specification;

namespace V8.Domain.Core
{
    public class BaseService<TEntity> : IDisposable, IBaseService<TEntity> where TEntity : class
    {
        private readonly IBaseRepository<TEntity> _baseRepository;

        public BaseService(IBaseRepository<TEntity> baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public int Add(TEntity entity)
        {
            return _baseRepository.Add(entity);
        }

        public int AddRange(IEnumerable<TEntity> list)
        {
            return _baseRepository.AddRange(list);
        }

        public void Delete(int id)
        {
            _baseRepository.Delete(id);
        }

        public void Dispose()
        {
            _baseRepository.Dispose();
        }

        public IEnumerable<TEntity> FindWithSpecificationPattern(ISpecification<TEntity> specification = null)
        {
            return _baseRepository.FindWithSpecificationPattern(specification);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _baseRepository.GetAll();
        }

        public TEntity GetByid(int id)
        {
            return _baseRepository.GetByid(id);
        }

        public bool Update(TEntity entity)
        {
            return _baseRepository.Update(entity);
        }
    }
}
