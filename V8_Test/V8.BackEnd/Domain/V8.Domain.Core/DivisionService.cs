﻿using System;
using System.Collections.Generic;
using System.Text;
using V8.Domain.Entities;
using V8.Domain.Interfaces.Repositories;
using V8.Domain.Interfaces.Services;

namespace V8.Domain.Core
{
    public class DivisionService : BaseService<Division>, IDivisionService
    {
        private readonly IDivisionRepository _divisionRepository;

        public DivisionService(IDivisionRepository divisionRepository) : base(divisionRepository)
        {
            _divisionRepository = divisionRepository;
        }
    }
}
