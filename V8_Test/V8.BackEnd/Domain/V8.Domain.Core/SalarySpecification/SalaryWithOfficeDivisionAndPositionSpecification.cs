﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using V8.Domain.Entities;

namespace V8.Domain.Core.SalarySpecification
{
    public class SalaryWithOfficeDivisionAndPositionSpecification : BaseSpecification<Salary>
    {
        public SalaryWithOfficeDivisionAndPositionSpecification() : base()
        {
            AddInclude(x => x.Office);
            AddInclude(x => x.Division);
            AddInclude(x => x.Position);
        }
    }
}
