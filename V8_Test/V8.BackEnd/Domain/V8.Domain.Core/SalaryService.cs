﻿using System;
using System.Collections.Generic;
using System.Text;
using V8.Domain.Entities;
using V8.Domain.Interfaces.Repositories;
using V8.Domain.Interfaces.Services;

namespace V8.Domain.Core
{
    public class SalaryService : BaseService<Salary>, ISalaryService
    {
        private readonly ISalaryRepository _salaryRepository;

        public SalaryService(ISalaryRepository salaryRepository) : base(salaryRepository)
        {
            _salaryRepository = salaryRepository;
        }

        public IEnumerable<Salary> FilterLastSalarys(int IdOffice, int IdDivision, int IdPosition)
        {
            return _salaryRepository.FilterLastSalarys(IdOffice, IdDivision, IdPosition);
        }

        public IEnumerable<Salary> GetLastSalarys()
        {
            return _salaryRepository.GetLastSalarys();
        }

        public IEnumerable<Salary> GetLastThreeSalarysWithBonoCalculated(string employeeCode)
        {
            return _salaryRepository.GetLastThreeSalarysWithBonoCalculated(employeeCode);
        }
    }
}
