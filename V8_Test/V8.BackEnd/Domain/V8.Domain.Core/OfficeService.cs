﻿using System;
using System.Collections.Generic;
using System.Text;
using V8.Domain.Entities;
using V8.Domain.Interfaces.Repositories;
using V8.Domain.Interfaces.Services;

namespace V8.Domain.Core
{
    public class OfficeService : BaseService<Office>, IOfficeService
    {
        private readonly IOfficeRepository _officeRepository;

        public OfficeService(IOfficeRepository officeRepository) : base(officeRepository)
        {
            _officeRepository = officeRepository;
        }
    }
}
