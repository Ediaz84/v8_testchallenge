﻿using System;
using System.Collections.Generic;
using System.Text;
using V8.Domain.Entities;
using V8.Domain.Interfaces.Repositories;
using V8.Infraestructure.Data.Context;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace V8.Infraestructure.Data.Repositories
{
    public class SalaryRepository : BaseRepository<Salary>, ISalaryRepository
    {
        private readonly ApplicationDbContext _context;
        public SalaryRepository(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<Salary> FilterLastSalarys(int IdOffice, int IdDivision, int IdPosition)
        {
            var salarys = (from s in _context.Salarys
                           join o in _context.Offices on s.IdOffice equals o.Id
                           join d in _context.Divisions on s.IdDivision equals d.Id
                           join p in _context.Positions on s.IdPosition equals p.Id
                           select s);

            if(IdOffice != -1)
            {
                salarys = salarys.Where(x => x.IdOffice == IdOffice);
            }

            if(IdDivision != -1)
            {
                salarys = salarys.Where(x => x.IdDivision == IdDivision);
            }

            if(IdPosition != -1)
            {
                salarys = salarys.Where(x => x.IdPosition == IdPosition);
            }

            var filterSalarys = from s in salarys.AsEnumerable()
                                 join o in _context.Offices.AsEnumerable() on s.IdOffice equals o.Id
                                 join d in _context.Divisions.AsEnumerable() on s.IdDivision equals d.Id
                                 join p in _context.Positions.AsEnumerable() on s.IdPosition equals p.Id
                                 group s by s.EmployeeCode into grp
                                 select grp.OrderByDescending(x => x.Year).ThenByDescending(y => y.Month).FirstOrDefault();

            return filterSalarys.ToList();
        }

        public IEnumerable<Salary> GetLastSalarys()
        {
            var salarys = (from s in _context.Salarys.AsEnumerable()
                           join o in _context.Offices.AsEnumerable() on s.IdOffice equals o.Id
                           join d in _context.Divisions.AsEnumerable() on s.IdDivision equals d.Id
                           join p in _context.Positions.AsEnumerable() on s.IdPosition equals p.Id
                           group s by s.EmployeeCode into grp
                           select grp.OrderByDescending(x => x.Year).ThenByDescending(y => y.Month).FirstOrDefault());

            return salarys.ToList();
        }

        public IEnumerable<Salary> GetLastThreeSalarysWithBonoCalculated(string employeeCode)
        {
            var salarys = (from s in _context.Salarys.AsEnumerable()
                           join d in _context.Divisions.AsEnumerable() on s.IdDivision equals d.Id
                           join p in _context.Positions.AsEnumerable() on s.IdPosition equals p.Id
                           where s.EmployeeCode == employeeCode
                           select s)
                           .OrderByDescending(x => x.Year)
                           .ThenByDescending(x => x.Month)
                           .Take(3)
                           .ToList();

            IList<Salary> resultList = new List<Salary>();

            for (int i=0; i<salarys.Count; i++)
            {
                if(salarys[0].Month - i == salarys[i].Month)
                {
                    resultList.Add(salarys[i]);
                }
            }
            return resultList;
        }
    }
}
