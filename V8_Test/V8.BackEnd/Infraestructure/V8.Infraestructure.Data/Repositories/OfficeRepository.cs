﻿using System;
using System.Collections.Generic;
using System.Text;
using V8.Domain.Entities;
using V8.Domain.Interfaces.Repositories;
using V8.Infraestructure.Data.Context;

namespace V8.Infraestructure.Data.Repositories
{
    public class OfficeRepository : BaseRepository<Office>, IOfficeRepository
    {
        private readonly ApplicationDbContext _context;
        public OfficeRepository(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
