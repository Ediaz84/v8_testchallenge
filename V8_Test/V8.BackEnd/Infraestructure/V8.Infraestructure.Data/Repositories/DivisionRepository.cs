﻿using System;
using System.Collections.Generic;
using System.Text;
using V8.Domain.Entities;
using V8.Domain.Interfaces.Repositories;
using V8.Infraestructure.Data.Context;

namespace V8.Infraestructure.Data.Repositories
{
    public class DivisionRepository : BaseRepository<Division>, IDivisionRepository
    {
        private readonly ApplicationDbContext _context;
        public DivisionRepository(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
