﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using V8.Domain.Entities;

namespace V8.Infraestructure.Data.Context
{
    public partial class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() { }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            Database.SetCommandTimeout(int.MaxValue);
        }

        public DbSet<Salary> Salarys { get; set; }
        public DbSet<Office> Offices { get; set; }
        public DbSet<Division> Divisions { get; set; }
        public DbSet<Position> Positions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Salary>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.Property(e => e.EmployeeCode)
                .HasMaxLength(20)
                .IsUnicode(false);

                entity.Property(e => e.EmployeeName)
                .HasMaxLength(300)
                .IsUnicode(false);

                entity.Property(e => e.EmployeeSurname)
                .HasMaxLength(300)
                .IsUnicode(false);

                entity.Property(e => e.BeginDate).HasColumnType("date");

                entity.Property(e => e.Birthday).HasColumnType("date");

                entity.Property(e => e.IdentificationNumber)
                .HasMaxLength(20)
                .IsUnicode(false);

                entity.Property(e => e.BaseSalary).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ProductionBonus).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CompensationBonus).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Commission).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Contributions).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.Office)
                .WithMany(p => p.Salarys)
                .HasForeignKey(d => d.IdOffice)
                .HasConstraintName("FK_Salary_Office");

                entity.HasOne(d => d.Position)
                .WithMany(p => p.Salarys)
                .HasForeignKey(d => d.IdPosition)
                .HasConstraintName("FK_Salary_Position");

                entity.HasOne(d => d.Division)
                .WithMany(p => p.Salarys)
                .HasForeignKey(d => d.IdDivision)
                .HasConstraintName("FK_Salary_Division");
            });

            modelBuilder.Entity<Office>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.Property(e => e.Name)
                .HasMaxLength(100)
                .IsUnicode(false);
            });

            modelBuilder.Entity<Division>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.Property(e => e.Name)
                .HasMaxLength(100)
                .IsUnicode(false);
            });

            modelBuilder.Entity<Position>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.Property(e => e.Name)
                .HasMaxLength(100)
                .IsUnicode(false);
            });
        }
    }
}
