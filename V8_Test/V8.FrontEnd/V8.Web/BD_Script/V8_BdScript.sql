USE [V8_Test]
GO
/****** Object:  Table [dbo].[Division]    Script Date: 05/05/2021 9:10:12 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Division](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Division] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Office]    Script Date: 05/05/2021 9:10:13 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Office](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Office] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Position]    Script Date: 05/05/2021 9:10:13 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Position](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Position] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Salary]    Script Date: 05/05/2021 9:10:13 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Salary](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Year] [int] NULL,
	[Month] [int] NULL,
	[IdOffice] [int] NULL,
	[EmployeeCode] [nvarchar](10) NULL,
	[EmployeeName] [nvarchar](150) NULL,
	[EmployeeSurname] [nvarchar](150) NULL,
	[IdDivision] [int] NULL,
	[IdPosition] [int] NULL,
	[Grade] [int] NULL,
	[BeginDate] [date] NULL,
	[Birthday] [date] NULL,
	[IdentificationNumber] [nvarchar](10) NULL,
	[BaseSalary] [decimal](18, 2) NULL,
	[ProductionBonus] [decimal](18, 2) NULL,
	[CompensationBonus] [decimal](18, 2) NULL,
	[Commission] [decimal](18, 2) NULL,
	[Contributions] [decimal](18, 2) NULL,
 CONSTRAINT [PK_Salary] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Division] ([Id], [Name]) VALUES (1, N'OPERATION')
INSERT [dbo].[Division] ([Id], [Name]) VALUES (2, N'SALES')
INSERT [dbo].[Division] ([Id], [Name]) VALUES (3, N'MARKETING')
INSERT [dbo].[Division] ([Id], [Name]) VALUES (4, N'CUSTOMER CARE')
GO
INSERT [dbo].[Office] ([Id], [Name]) VALUES (1, N'A')
INSERT [dbo].[Office] ([Id], [Name]) VALUES (2, N'C')
INSERT [dbo].[Office] ([Id], [Name]) VALUES (3, N'D')
INSERT [dbo].[Office] ([Id], [Name]) VALUES (4, N'ZZ')
GO
INSERT [dbo].[Position] ([Id], [Name]) VALUES (1, N'CARGO MANAGER')
INSERT [dbo].[Position] ([Id], [Name]) VALUES (2, N'HEAD OF CARGO')
INSERT [dbo].[Position] ([Id], [Name]) VALUES (3, N'CARGO ASSISTANT')
INSERT [dbo].[Position] ([Id], [Name]) VALUES (4, N'SALES MANAGER')
INSERT [dbo].[Position] ([Id], [Name]) VALUES (5, N'ACCOUNT EXECUTIVE')
INSERT [dbo].[Position] ([Id], [Name]) VALUES (6, N'MARKETING ASSISTANT')
INSERT [dbo].[Position] ([Id], [Name]) VALUES (7, N'CUSTOMER DIRECTOR')
INSERT [dbo].[Position] ([Id], [Name]) VALUES (8, N'CUSTOMER ASSISTANT')
GO
SET IDENTITY_INSERT [dbo].[Salary] ON 

INSERT [dbo].[Salary] ([Id], [Year], [Month], [IdOffice], [EmployeeCode], [EmployeeName], [EmployeeSurname], [IdDivision], [IdPosition], [Grade], [BeginDate], [Birthday], [IdentificationNumber], [BaseSalary], [ProductionBonus], [CompensationBonus], [Commission], [Contributions]) VALUES (1, 2020, 10, 2, N'10001222', N'MIKE', N'JAMES', 1, 1, 18, CAST(N'2020-05-11' AS Date), CAST(N'1990-03-15' AS Date), N'45642132', CAST(2799.45 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(3215.63 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)))
INSERT [dbo].[Salary] ([Id], [Year], [Month], [IdOffice], [EmployeeCode], [EmployeeName], [EmployeeSurname], [IdDivision], [IdPosition], [Grade], [BeginDate], [Birthday], [IdentificationNumber], [BaseSalary], [ProductionBonus], [CompensationBonus], [Commission], [Contributions]) VALUES (2, 2020, 9, 2, N'10001222', N'MIKE', N'JAMES', 1, 1, 18, CAST(N'2020-05-11' AS Date), CAST(N'1990-03-15' AS Date), N'45642132', CAST(2799.45 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(3263.99 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)))
INSERT [dbo].[Salary] ([Id], [Year], [Month], [IdOffice], [EmployeeCode], [EmployeeName], [EmployeeSurname], [IdDivision], [IdPosition], [Grade], [BeginDate], [Birthday], [IdentificationNumber], [BaseSalary], [ProductionBonus], [CompensationBonus], [Commission], [Contributions]) VALUES (3, 2020, 8, 3, N'10001222', N'MIKE', N'JAMES', 1, 2, 12, CAST(N'2020-05-11' AS Date), CAST(N'1990-03-15' AS Date), N'45642132', CAST(2799.45 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(998.45 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)))
INSERT [dbo].[Salary] ([Id], [Year], [Month], [IdOffice], [EmployeeCode], [EmployeeName], [EmployeeSurname], [IdDivision], [IdPosition], [Grade], [BeginDate], [Birthday], [IdentificationNumber], [BaseSalary], [ProductionBonus], [CompensationBonus], [Commission], [Contributions]) VALUES (4, 2020, 7, 3, N'10001222', N'MIKE', N'JAMES', 1, 2, 12, CAST(N'2020-05-11' AS Date), CAST(N'1990-03-15' AS Date), N'45642132', CAST(2799.45 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(998.45 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)))
INSERT [dbo].[Salary] ([Id], [Year], [Month], [IdOffice], [EmployeeCode], [EmployeeName], [EmployeeSurname], [IdDivision], [IdPosition], [Grade], [BeginDate], [Birthday], [IdentificationNumber], [BaseSalary], [ProductionBonus], [CompensationBonus], [Commission], [Contributions]) VALUES (5, 2020, 6, 1, N'10001222', N'MIKE', N'JAMES', 1, 3, 6, CAST(N'2020-05-11' AS Date), CAST(N'1990-03-15' AS Date), N'45642132', CAST(2799.45 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(4500.00 AS Decimal(18, 2)), CAST(1450.00 AS Decimal(18, 2)))
INSERT [dbo].[Salary] ([Id], [Year], [Month], [IdOffice], [EmployeeCode], [EmployeeName], [EmployeeSurname], [IdDivision], [IdPosition], [Grade], [BeginDate], [Birthday], [IdentificationNumber], [BaseSalary], [ProductionBonus], [CompensationBonus], [Commission], [Contributions]) VALUES (6, 2020, 5, 1, N'10001222', N'MIKE', N'JAMES', 1, 3, 6, CAST(N'2020-05-11' AS Date), CAST(N'1990-03-15' AS Date), N'45642132', CAST(2799.45 AS Decimal(18, 2)), CAST(1800.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), CAST(1600.00 AS Decimal(18, 2)))
INSERT [dbo].[Salary] ([Id], [Year], [Month], [IdOffice], [EmployeeCode], [EmployeeName], [EmployeeSurname], [IdDivision], [IdPosition], [Grade], [BeginDate], [Birthday], [IdentificationNumber], [BaseSalary], [ProductionBonus], [CompensationBonus], [Commission], [Contributions]) VALUES (7, 2020, 10, 4, N'10023001', N'KALI', N'PRASAD', 2, 4, 18, CAST(N'2020-09-09' AS Date), CAST(N'1980-01-25' AS Date), N'14598756', CAST(2995.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(3200.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(2100.00 AS Decimal(18, 2)))
INSERT [dbo].[Salary] ([Id], [Year], [Month], [IdOffice], [EmployeeCode], [EmployeeName], [EmployeeSurname], [IdDivision], [IdPosition], [Grade], [BeginDate], [Birthday], [IdentificationNumber], [BaseSalary], [ProductionBonus], [CompensationBonus], [Commission], [Contributions]) VALUES (8, 2020, 9, 4, N'10023001', N'KALI', N'PRASAD', 2, 4, 18, CAST(N'2020-09-09' AS Date), CAST(N'1980-01-25' AS Date), N'14598756', CAST(2995.00 AS Decimal(18, 2)), CAST(3200.00 AS Decimal(18, 2)), CAST(3200.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(2340.00 AS Decimal(18, 2)))
INSERT [dbo].[Salary] ([Id], [Year], [Month], [IdOffice], [EmployeeCode], [EmployeeName], [EmployeeSurname], [IdDivision], [IdPosition], [Grade], [BeginDate], [Birthday], [IdentificationNumber], [BaseSalary], [ProductionBonus], [CompensationBonus], [Commission], [Contributions]) VALUES (9, 2020, 10, 4, N'10023011', N'JANE', N'DOE', 2, 5, 12, CAST(N'2020-10-10' AS Date), CAST(N'1978-11-18' AS Date), N'1989863', CAST(3000.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)))
INSERT [dbo].[Salary] ([Id], [Year], [Month], [IdOffice], [EmployeeCode], [EmployeeName], [EmployeeSurname], [IdDivision], [IdPosition], [Grade], [BeginDate], [Birthday], [IdentificationNumber], [BaseSalary], [ProductionBonus], [CompensationBonus], [Commission], [Contributions]) VALUES (10, 2020, 7, 4, N'10023011', N'JANE', N'DOE', 2, 5, 12, CAST(N'2020-05-02' AS Date), CAST(N'1978-11-18' AS Date), N'1989863', CAST(2799.45 AS Decimal(18, 2)), CAST(3950.00 AS Decimal(18, 2)), CAST(2365.74 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)))
INSERT [dbo].[Salary] ([Id], [Year], [Month], [IdOffice], [EmployeeCode], [EmployeeName], [EmployeeSurname], [IdDivision], [IdPosition], [Grade], [BeginDate], [Birthday], [IdentificationNumber], [BaseSalary], [ProductionBonus], [CompensationBonus], [Commission], [Contributions]) VALUES (11, 2020, 6, 4, N'10023011', N'JANE', N'DOE', 3, 6, 11, CAST(N'2020-05-02' AS Date), CAST(N'1978-11-18' AS Date), N'1989863', CAST(2799.45 AS Decimal(18, 2)), CAST(2666.00 AS Decimal(18, 2)), CAST(2365.74 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Salary] ([Id], [Year], [Month], [IdOffice], [EmployeeCode], [EmployeeName], [EmployeeSurname], [IdDivision], [IdPosition], [Grade], [BeginDate], [Birthday], [IdentificationNumber], [BaseSalary], [ProductionBonus], [CompensationBonus], [Commission], [Contributions]) VALUES (12, 2020, 5, 4, N'10023011', N'JANE', N'DOE', 3, 6, 11, CAST(N'2020-05-02' AS Date), CAST(N'1978-11-18' AS Date), N'1989863', CAST(2799.45 AS Decimal(18, 2)), CAST(2666.00 AS Decimal(18, 2)), CAST(2365.74 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Salary] ([Id], [Year], [Month], [IdOffice], [EmployeeCode], [EmployeeName], [EmployeeSurname], [IdDivision], [IdPosition], [Grade], [BeginDate], [Birthday], [IdentificationNumber], [BaseSalary], [ProductionBonus], [CompensationBonus], [Commission], [Contributions]) VALUES (13, 2020, 10, 3, N'10001001', N'ANN', N'WHITAKER', 4, 7, 18, CAST(N'2020-05-16' AS Date), CAST(N'1992-06-21' AS Date), N'8563217', CAST(5000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(3200.00 AS Decimal(18, 2)), CAST(2478.96 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)))
INSERT [dbo].[Salary] ([Id], [Year], [Month], [IdOffice], [EmployeeCode], [EmployeeName], [EmployeeSurname], [IdDivision], [IdPosition], [Grade], [BeginDate], [Birthday], [IdentificationNumber], [BaseSalary], [ProductionBonus], [CompensationBonus], [Commission], [Contributions]) VALUES (14, 2020, 9, 3, N'10001001', N'ANN', N'WHITAKER', 4, 7, 18, CAST(N'2020-05-16' AS Date), CAST(N'1992-06-21' AS Date), N'8563217', CAST(5000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(3200.00 AS Decimal(18, 2)), CAST(2677.88 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)))
INSERT [dbo].[Salary] ([Id], [Year], [Month], [IdOffice], [EmployeeCode], [EmployeeName], [EmployeeSurname], [IdDivision], [IdPosition], [Grade], [BeginDate], [Birthday], [IdentificationNumber], [BaseSalary], [ProductionBonus], [CompensationBonus], [Commission], [Contributions]) VALUES (15, 2020, 8, 3, N'10001001', N'ANN', N'WHITAKER', 4, 7, 14, CAST(N'2020-05-16' AS Date), CAST(N'1992-06-21' AS Date), N'8563217', CAST(3200.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(998.45 AS Decimal(18, 2)), CAST(1845.66 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)))
INSERT [dbo].[Salary] ([Id], [Year], [Month], [IdOffice], [EmployeeCode], [EmployeeName], [EmployeeSurname], [IdDivision], [IdPosition], [Grade], [BeginDate], [Birthday], [IdentificationNumber], [BaseSalary], [ProductionBonus], [CompensationBonus], [Commission], [Contributions]) VALUES (16, 2020, 7, 3, N'10001001', N'ANN', N'WHITAKER', 4, 7, 14, CAST(N'2020-05-16' AS Date), CAST(N'1992-06-21' AS Date), N'8563217', CAST(2799.45 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(998.45 AS Decimal(18, 2)), CAST(1944.22 AS Decimal(18, 2)), CAST(600.00 AS Decimal(18, 2)))
INSERT [dbo].[Salary] ([Id], [Year], [Month], [IdOffice], [EmployeeCode], [EmployeeName], [EmployeeSurname], [IdDivision], [IdPosition], [Grade], [BeginDate], [Birthday], [IdentificationNumber], [BaseSalary], [ProductionBonus], [CompensationBonus], [Commission], [Contributions]) VALUES (17, 2020, 6, 1, N'10001001', N'ANN', N'WHITAKER', 4, 8, 6, CAST(N'2020-05-16' AS Date), CAST(N'1992-06-21' AS Date), N'8563217', CAST(2799.45 AS Decimal(18, 2)), CAST(1899.00 AS Decimal(18, 2)), CAST(2475.54 AS Decimal(18, 2)), CAST(896.22 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Salary] ([Id], [Year], [Month], [IdOffice], [EmployeeCode], [EmployeeName], [EmployeeSurname], [IdDivision], [IdPosition], [Grade], [BeginDate], [Birthday], [IdentificationNumber], [BaseSalary], [ProductionBonus], [CompensationBonus], [Commission], [Contributions]) VALUES (18, 2020, 5, 1, N'10001001', N'ANN', N'WHITAKER', 4, 8, 6, CAST(N'2020-05-16' AS Date), CAST(N'1992-06-21' AS Date), N'8563217', CAST(2799.45 AS Decimal(18, 2)), CAST(1852.21 AS Decimal(18, 2)), CAST(2475.54 AS Decimal(18, 2)), CAST(801.85 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[Salary] OFF
GO
ALTER TABLE [dbo].[Salary]  WITH CHECK ADD  CONSTRAINT [FK_Salary_Division] FOREIGN KEY([IdDivision])
REFERENCES [dbo].[Division] ([Id])
GO
ALTER TABLE [dbo].[Salary] CHECK CONSTRAINT [FK_Salary_Division]
GO
ALTER TABLE [dbo].[Salary]  WITH CHECK ADD  CONSTRAINT [FK_Salary_Office] FOREIGN KEY([IdOffice])
REFERENCES [dbo].[Office] ([Id])
GO
ALTER TABLE [dbo].[Salary] CHECK CONSTRAINT [FK_Salary_Office]
GO
ALTER TABLE [dbo].[Salary]  WITH CHECK ADD  CONSTRAINT [FK_Salary_Position] FOREIGN KEY([IdPosition])
REFERENCES [dbo].[Position] ([Id])
GO
ALTER TABLE [dbo].[Salary] CHECK CONSTRAINT [FK_Salary_Position]
GO
