﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace V8.Web.Exceptions
{
    public class NameOrSurnameDifferentThanTheAlreadyAddedException : Exception
    {
        public NameOrSurnameDifferentThanTheAlreadyAddedException()
        {

        }

        public NameOrSurnameDifferentThanTheAlreadyAddedException(string employeeFullName)
            : base($"Employee Full Name {employeeFullName} is different than the one already added to the salary detail.")
        {

        }
    }
}
