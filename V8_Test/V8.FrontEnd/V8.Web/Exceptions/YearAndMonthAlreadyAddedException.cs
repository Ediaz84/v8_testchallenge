﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace V8.Web.Exceptions
{
    [Serializable]
    public class YearAndMonthAlreadyAddedException : Exception
    {
        public YearAndMonthAlreadyAddedException()
        {

        }

        public YearAndMonthAlreadyAddedException(string employeeCode, int year, int month)
            : base($"Salary for EmployeeCode {employeeCode} belonging to period: year {year} and month {month} already added to detail salarys.")
        {

        }
    }
}
