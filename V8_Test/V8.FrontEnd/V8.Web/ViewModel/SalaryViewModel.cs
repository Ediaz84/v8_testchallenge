﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace V8.Web.ViewModel
{
    public class SalaryViewModel
    {
		public SalaryViewModel()
        {
			listOfSalarys = new List<SalaryDetailViewModel>();
        }

		[Required(ErrorMessage = "Year is required.")]
		[RegularExpression("^([0-9][0-9][0-9][0-9])$", ErrorMessage = "Year must have 4 digits.")]
		public int Year { get; set; }
		[Required(ErrorMessage = "Month is required.")]
		[RegularExpression("^([1-9]|1[012])$", ErrorMessage = "Only allowed numbers between 1 and 12.")]
		public int Month { get; set; }
		[Required(ErrorMessage = "Office is required.")]
		[DisplayName("Office")]
		public int IdOffice { get; set; }
		public IList<OfficeViewModel> OfficeList { get; set; }
		[Required(ErrorMessage = "Employee Code is required.")]
		[DisplayName("Employee Code")]
		[MaxLength(8, ErrorMessage = "Max Length is 8 digits")]
		[RegularExpression("^([0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9])", ErrorMessage = "Employee Code must have 8 digits.")]
		public string EmployeeCode { get; set; }
		[Required(ErrorMessage = "Employee Name is required.")]
		[DisplayName("Employee Name")]
		[MaxLength(300, ErrorMessage = "Max Length is 300 characters")]
		[RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Only allowed letters.")]
		public string EmployeeName { get; set; }
		[Required(ErrorMessage = "Employee Surname is required.")]
		[DisplayName("Employee Surname")]
		[MaxLength(300, ErrorMessage = "Max Length is 300 characters")]
		[RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Only allowed letters.")]
		public string EmployeeSurname { get; set; }
		[Required(ErrorMessage = "Division is required.")]
		[DisplayName("Division")]
		public int IdDivision { get; set; }
		public IList<DivisionViewModel> DivisionList { get; set; }
		[Required(ErrorMessage = "Position is required.")]
		[DisplayName("Position")]
		public int IdPosition { get; set; }
		public IList<PositionViewModel> PositionList { get; set; }
		[Required(ErrorMessage = "Grade is required.")]
		[RegularExpression("^([1-9]|[1][0-9]|2[0])$", ErrorMessage = "Only allowed numbers between 1 and 20.")]
		public int Grade { get; set; }
		[Required(ErrorMessage = "Begin Date is required.")]
		[DisplayName("Begin Date")]
		[DisplayFormat(DataFormatString = "{dd/MM/yyyy}")]
		[RegularExpression("^(?:[012]?[0-9]|3[01])[./-](?:0?[1-9]|1[0-2])[./-](?:[0-9]{2}){1,2}$", ErrorMessage = "Dates must have format dd/MM/yyyy")]
		public DateTime BeginDate { get; set; }
		[Required(ErrorMessage = "Birthday is required.")]
		[DisplayFormat(DataFormatString = "{dd/MM/yyyy}")]
		[RegularExpression("^(?:[012]?[0-9]|3[01])[./-](?:0?[1-9]|1[0-2])[./-](?:[0-9]{2}){1,2}$", ErrorMessage = "Dates must have format dd/MM/yyyy")]
		public DateTime Birthday { get; set; }
		[Required(ErrorMessage = "ID Number is required.")]
		[DisplayName("ID Number")]
		[MaxLength(8, ErrorMessage = "Max Length is 8 digits")]
		[RegularExpression("^([0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9])$", ErrorMessage = "ID Number must have 8 digits.")]
		public string IdentificationNumber { get; set; }
		[Required(ErrorMessage = "Base Salary required.")]
		[DisplayName("Base Salary")]
		[RegularExpression("^(\\d{1,8})?(\\.\\d{1,2})?$", ErrorMessage = "Numeric value max length is 4 with/out 2 decimal digits.")]
		[Range(1.00, 9999.00, ErrorMessage = "Please enter a Amount between 1.00 and 9999.00")]
		public decimal BaseSalary { get; set; }
		[Required(ErrorMessage = "Production Bonus is required.")]
		[DisplayName("Production Bonus")]
		[RegularExpression("^(\\d{1,8})?(\\.\\d{1,2})?$", ErrorMessage = "Numeric value max length is 4 with/out 2 decimal digits.")]
		public decimal ProductionBonus { get; set; }
		[Required(ErrorMessage = "Compensation Bonus is required.")]
		[DisplayName("Compensation Bonus")]
		[RegularExpression("^(\\d{1,8})?(\\.\\d{1,2})?$", ErrorMessage = "Numeric value max length is 4 with/out 2 decimal digits.")]
		public decimal CompensationBonus { get; set; }
		[Required(ErrorMessage = "Commission is required.")]
		[RegularExpression("^(\\d{1,8})?(\\.\\d{1,2})?$", ErrorMessage = "Numeric value max length is 4 with/out 2 decimal digits.")]
		public decimal Commission { get; set; }
		[Required(ErrorMessage = "Contributions is required.")]
		[RegularExpression("^(\\d{1,8})?(\\.\\d{1,2})?$", ErrorMessage = "Numeric value max length is 4 with/out 2 decimal digits.")]
		public decimal Contributions { get; set; }
		public IList<SalaryDetailViewModel> listOfSalarys { get; set; }
	}
}
