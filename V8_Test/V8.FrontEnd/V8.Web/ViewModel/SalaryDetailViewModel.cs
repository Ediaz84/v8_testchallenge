﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace V8.Web.ViewModel
{
    public class SalaryDetailViewModel
    {
		public string BeginDateShortDateString
		{
			get
			{
				return BeginDate.ToString("dd/MM/yyyy");
			}
		}
		public string BirthdayShortDateString
		{
			get
			{
				return Birthday.ToString("dd/MM/yyyy");
			}
		}
		public int Year { get; set; }
		public int Month { get; set; }
		public int IdOffice { get; set; }
		public string EmployeeCode { get; set; }
		public string EmployeeName { get; set; }
		public string EmployeeSurname { get; set; }
		public int IdDivision { get; set; }
		public int IdPosition { get; set; }
		public int Grade { get; set; }
		public DateTime BeginDate { get; set; }
		public DateTime Birthday { get; set; }
		public string IdentificationNumber { get; set; }
		public decimal BaseSalary { get; set; }
		public decimal ProductionBonus { get; set; }
		public decimal CompensationBonus { get; set; }
		public decimal Commission { get; set; }
		public decimal Contributions { get; set; }
		public string OfficeName { get; set; }
		public string DivisionName { get; set; }
		public string PositionName { get; set; }
	}
}
