﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace V8.Web.ViewModel
{
    public class CalculatedSalaryBonoViewModel
    {
		public CalculatedSalaryBonoViewModel()
		{
			listOfCalculatedSalarys = new List<ListCalculatedSalaryViewModel>();
		}

		[Required(ErrorMessage = "Employee Code is required.")]
		[DisplayName("Employee Code")]
		[MaxLength(8, ErrorMessage = "Max Length is 8 digits")]
		[RegularExpression("^([0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9])", ErrorMessage = "Employee Code must have 8 digits.")]
		public string EmployeeCode { get; set; }
		[DisplayName("Employee Full Name")]
		public string EmployeeFullName { get; set; }
		public string Division { get; set; }
		public string Position { get; set; }
		[DisplayName("Begin Date")]
		public DateTime BeginDate { get; set; }
		public DateTime Birthday { get; set; }
		[DisplayName("ID Number")]
		public string IdentificationNumber { get; set; }
		[DisplayName("Total Salary")]
		public decimal TotalSalary { get; set; }
		public IList<ListCalculatedSalaryViewModel> listOfCalculatedSalarys { get; set; }
	}
}
