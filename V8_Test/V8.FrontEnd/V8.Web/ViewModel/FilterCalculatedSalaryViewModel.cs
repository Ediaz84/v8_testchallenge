﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace V8.Web.ViewModel
{
    public class FilterCalculatedSalaryViewModel
    {
        public int IdOffice { get; set; }
        public int IdPosition { get; set; }
        public int IdDivision { get; set; }
    }
}
