﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace V8.Web.ViewModel
{
    public class ListSalaryViewModel
    {
		public string BeginDateShortDateString
		{
			get
			{
				return BeginDate.ToString("dd/MM/yyyy");
			}
		}
		public string BirthdayShortDateString
		{
			get
			{
				return Birthday.ToString("dd/MM/yyyy");
			}
		}
		public int Id { get; set; }
		public int Year { get; set; }
		public int Month { get; set; }
		[DisplayName("Employee Code")]
		public string EmployeeCode { get; set; }
		[DisplayName("Employee Name")]
		public string EmployeeName { get; set; }
		[DisplayName("Employee Surname")]
		public string EmployeeSurname { get; set; }
		public int Grade { get; set; }
		[DisplayName("Begin Date")]
		public DateTime BeginDate { get; set; }
		public DateTime Birthday { get; set; }
		[DisplayName("ID Number")]
		public string IdentificationNumber { get; set; }
		[DisplayName("Base Salary")]
		public decimal BaseSalary { get; set; }
		[DisplayName("Production Bonus")]
		public decimal ProductionBonus { get; set; }
		[DisplayName("Compensation Bonus")]
		public decimal CompensationBonus { get; set; }
		[DisplayName("Commission")]
		public decimal Commission { get; set; }
		public decimal Contributions { get; set; }
		public string Division { get; set; }
		public string Office { get; set; }
		public string Position { get; set; }
	}
}
