﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace V8.Web.ViewModel
{
    public class ListCalculatedSalaryViewModel
    {
		public string BeginDateShortDateString
		{
			get
			{
				return BeginDate.ToString("dd/MM/yyyy");
			}
		}
		public string BirthdayShortDateString
		{
			get
			{
				return Birthday.ToString("dd/MM/yyyy");
			}
		}

		public string EmployeeCode { get; set; }
		public string EmployeeFullName { get; set; }
		public string Office { get; set; }
		public string Division { get; set; }
		public string Position { get; set; }
		public DateTime BeginDate { get; set; }
		public DateTime Birthday { get; set; }
		public string IdentificationNumber { get; set; }
		public decimal TotalSalary { get; set; }
	}
}
