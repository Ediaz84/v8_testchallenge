﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace V8.Web.ViewModel
{
    public class CalculatedSalaryViewModel
    {
		public CalculatedSalaryViewModel()
		{
			listOfCalculatedSalarys = new List<ListCalculatedSalaryViewModel>();
		}

		[DisplayName("Employee Code")]
		public string EmployeeCode { get; set; }
		[DisplayName("Employee Full Name")]
		public string EmployeeFullName { get; set; }
		public string Division { get; set; }
		public string Position { get; set; }
		public string Office { get; set; }
		[DisplayName("Begin Date")]
		public DateTime BeginDate { get; set; }
		public DateTime Birthday { get; set; }
		[DisplayName("ID Number")]
		public string IdentificationNumber { get; set; }
		[DisplayName("Total Salary")]
		public decimal TotalSalary { get; set; }
		[DisplayName("Division")]
		public int IdDivision { get; set; }
		public IList<DivisionViewModel> DivisionList { get; set; }
		[DisplayName("Position")]
		public int IdPosition { get; set; }
		public IList<PositionViewModel> PositionList { get; set; }
		[DisplayName("Office")]
		public int IdOffice { get; set; }
		public IList<OfficeViewModel> OfficeList { get; set; }
		public IList<ListCalculatedSalaryViewModel> listOfCalculatedSalarys { get; set; }
	}
}
