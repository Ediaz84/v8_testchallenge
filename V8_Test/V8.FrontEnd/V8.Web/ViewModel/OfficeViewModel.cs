﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace V8.Web.ViewModel
{
    public class OfficeViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
