﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using V8.Web.Exceptions;
using V8.Web.Helpers;
using V8.Web.ViewModel;

namespace V8.Web.Controllers
{
    public class SalaryController : Controller
    {
        private readonly IHttpClientFactory _clientFactory;

        public SalaryController(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory ?? throw new ArgumentNullException(nameof(clientFactory));
        }
        // GET: SalaryController
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var request = new HttpRequestMessage(HttpMethod.Get,
                "http://localhost:5142/api/Salary");
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("User-Agent", "HttpClientFactory-Sample");

            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            IEnumerable<ListSalaryViewModel> lisOfSalarys;

            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                lisOfSalarys = JsonConvert.DeserializeObject<IEnumerable<ListSalaryViewModel>>(responseContent);
            }
            else
            {
                lisOfSalarys = Array.Empty<ListSalaryViewModel>();
            }
            return View("ListOfSalarys", lisOfSalarys);
        }

        // GET: SalaryController/Create
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            SalaryViewModel salary = new SalaryViewModel
            {
                OfficeList = await GetOfficesAsync(),
                PositionList = await GetPositionsAsync(),
                DivisionList = await GetDivisionsAsync()
            };

            HttpContext.Session.SetObjectAsJson("SalaryInit", salary);

            return View("PostOfSalarys", salary);
        }

        // POST: SalaryController/AddSalary
        [HttpPost]
        public async Task<IActionResult> AddSalary(SalaryViewModel salary)
        {
            var salaryDetailViewModel = await MapToSalaryDetailViewModel(salary);
            var salaryDetailAdded = HttpContext.Session.GetObjectFromJson<List<SalaryDetailViewModel>>("SalaryDetail");

            try
            {
                if (salaryDetailAdded != null && salaryDetailAdded.Count > 0)
                {
                    salaryDetailAdded.ForEach(x =>
                    {                        
                        if (x.EmployeeCode == salary.EmployeeCode) 
                        {
                            if (x.Year == salary.Year && x.Month == salary.Month)
                                throw new YearAndMonthAlreadyAddedException(salary.EmployeeCode, salary.Year, salary.Month);
                            if (!x.EmployeeName.Equals(salary.EmployeeName) || !x.EmployeeSurname.Equals(salary.EmployeeSurname))
                                throw new NameOrSurnameDifferentThanTheAlreadyAddedException(salary.EmployeeName + " " + salary.EmployeeSurname);
                        }
                    });

                    salary.listOfSalarys = salaryDetailAdded;
                    salary.listOfSalarys.Add(salaryDetailViewModel);
                    HttpContext.Session.SetObjectAsJson("SalaryDetail", salary.listOfSalarys);
                }
                else
                {
                    salary.listOfSalarys.Add(salaryDetailViewModel);
                    HttpContext.Session.SetObjectAsJson("SalaryDetail", salary.listOfSalarys);
                }

                return PartialView("_AddSalary", salary);
            }
            catch (YearAndMonthAlreadyAddedException ex)
            {
                return Conflict(ex.Message);
            }
            catch (NameOrSurnameDifferentThanTheAlreadyAddedException ex)
            {
                return Conflict(ex.Message);
            }
        }

        // POST: SalaryController/Create
        [HttpPost]
        public async Task<IActionResult> Create(IList<SalaryDetailViewModel> listOfSalarys)
        {
            var newListOfSalarysJson = JsonConvert.SerializeObject(listOfSalarys);
            var request = new HttpRequestMessage(HttpMethod.Post,
                "http://localhost:5142/api/Salary");
            request.Content = new StringContent(newListOfSalarysJson, Encoding.UTF8, "application/json");

            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            if(response.IsSuccessStatusCode)
            {
                HttpContext.Session.Clear();
                ViewData["Result"] = "1";
                return RedirectToAction("Create", "Salary");
            }

            ViewData["Result"] = "0";
            var salary = HttpContext.Session.GetObjectFromJson<SalaryViewModel>("SalaryInit");
            if(salary == null)
            {
                salary = new SalaryViewModel
                {
                    OfficeList = await GetOfficesAsync(),
                    PositionList = await GetPositionsAsync(),
                    DivisionList = await GetDivisionsAsync()
                };
            }

            return View("PostOfSalarys", salary);
        }

        // GET: SalaryController/Create
        [HttpGet]
        public async Task<IActionResult> GetCalculatedSalarys()
        {
            var request = new HttpRequestMessage(HttpMethod.Get,
               "http://localhost:5142/api/Salary/Calculated");
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("User-Agent", "HttpClientFactory-Sample");

            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            CalculatedSalaryViewModel calculatedSalaryViewModel;

            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                var lisOfCalculatedSalarys = JsonConvert.DeserializeObject<IEnumerable<ListCalculatedSalaryViewModel>>(responseContent);
                calculatedSalaryViewModel = new CalculatedSalaryViewModel
                {
                    listOfCalculatedSalarys = lisOfCalculatedSalarys.ToList(),
                    OfficeList = await GetOfficesAsync(true),
                    DivisionList = await GetDivisionsAsync(true),
                    PositionList = await GetPositionsAsync(true)
                };
            }
            else
            {
                calculatedSalaryViewModel = null;
            }

            return View("ListOfCalculatedSalarys", calculatedSalaryViewModel);
        }

        // POST: SalaryController/SearchCalculatedSalarys
        [HttpPost]
        public async Task<IActionResult> FilterCalculatedSalarys(FilterCalculatedSalaryViewModel filterViewModel)
        {
            var newFilterCalculatedSalarysJson = JsonConvert.SerializeObject(filterViewModel);
            var request = new HttpRequestMessage(HttpMethod.Post,
               "http://localhost:5142/api/Salary/FilterCalculated");
            request.Content = new StringContent(newFilterCalculatedSalarysJson, Encoding.UTF8, "application/json");

            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            CalculatedSalaryViewModel calculatedSalaryViewModel;

            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                var lisOfCalculatedSalarys = JsonConvert.DeserializeObject<IEnumerable<ListCalculatedSalaryViewModel>>(responseContent);
                calculatedSalaryViewModel = new CalculatedSalaryViewModel
                {
                    listOfCalculatedSalarys = lisOfCalculatedSalarys.ToList(),
                    IdOffice = filterViewModel.IdOffice,
                    IdDivision = filterViewModel.IdDivision,
                    IdPosition = filterViewModel.IdPosition
                };
            }
            else
            {
                calculatedSalaryViewModel = null;
            }

            return PartialView("_CalculatedSalarys", calculatedSalaryViewModel);
        }

        // GET: SalaryController/GetBono
        [HttpGet]
        public IActionResult GetBono()
        {
            CalculatedSalaryBonoViewModel calculatedSalaryBonoViewModel = new CalculatedSalaryBonoViewModel();

            return View("ListOfCalculatedSalarysBono", calculatedSalaryBonoViewModel);
        }

        // POST: SalaryController/GetLastThreeCalculatedSalarys
        [HttpPost]
        public async Task<IActionResult> GetLastThreeCalculatedSalarys(FilterSalaryBonoViewModel filterSalaryBonoViewModel)
        {
            var newFilterCalculatedSalarysJson = JsonConvert.SerializeObject(filterSalaryBonoViewModel);
            var request = new HttpRequestMessage(HttpMethod.Post,
               "http://localhost:5142/api/Salary/Bono");
            request.Content = new StringContent(newFilterCalculatedSalarysJson, Encoding.UTF8, "application/json");

            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            CalculatedSalaryBonoViewModel calculatedSalaryBonoViewModel;

            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                var lisOfCalculatedSalarys = JsonConvert.DeserializeObject<IEnumerable<ListCalculatedSalaryViewModel>>(responseContent);
                calculatedSalaryBonoViewModel = new CalculatedSalaryBonoViewModel
                {
                    listOfCalculatedSalarys = lisOfCalculatedSalarys.ToList(),
                };
            }
            else
            {
                calculatedSalaryBonoViewModel = null;
            }

            return PartialView("_CalculatedSalarysBono", calculatedSalaryBonoViewModel);
        }

        private async Task<IList<OfficeViewModel>> GetOfficesAsync(bool allOptionRequired = false)
        {
            var request = new HttpRequestMessage(HttpMethod.Get,
                "http://localhost:5142/api/Office/All/"+allOptionRequired);
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("User-Agent", "HttpClientFactory-Sample");

            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            IList<OfficeViewModel> lisOfOffices;

            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                lisOfOffices = JsonConvert.DeserializeObject<IList<OfficeViewModel>>(responseContent);
            }
            else
            {
                lisOfOffices = Array.Empty<OfficeViewModel>();
            }

            return lisOfOffices;
        }

        private async Task<OfficeViewModel> GetOfficeByIdAsync(int id)
        {
            var request = new HttpRequestMessage(HttpMethod.Get,
                "http://localhost:5142/api/Office/"+id);
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("User-Agent", "HttpClientFactory-Sample");

            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            OfficeViewModel office;

            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                office = JsonConvert.DeserializeObject<OfficeViewModel>(responseContent);
            }
            else
            {
                office = null;
            }

            return office;
        }

        private async Task<IList<PositionViewModel>> GetPositionsAsync(bool allOptionRequired = false)
        {
            var request = new HttpRequestMessage(HttpMethod.Get,
                "http://localhost:5142/api/Position/All/"+allOptionRequired);
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("User-Agent", "HttpClientFactory-Sample");

            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            IList<PositionViewModel> lisOfPositions;

            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                lisOfPositions = JsonConvert.DeserializeObject<IList<PositionViewModel>>(responseContent);
            }
            else
            {
                lisOfPositions = Array.Empty<PositionViewModel>();
            }

            return lisOfPositions;
        }

        private async Task<PositionViewModel> GetPositionByIdAsync(int id)
        {
            var request = new HttpRequestMessage(HttpMethod.Get,
                "http://localhost:5142/api/Position/"+id);
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("User-Agent", "HttpClientFactory-Sample");

            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            PositionViewModel position;

            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                position = JsonConvert.DeserializeObject<PositionViewModel>(responseContent);
            }
            else
            {
                position = null;
            }

            return position;
        }

        private async Task<IList<DivisionViewModel>> GetDivisionsAsync(bool allOptionRequired = false)
        {
            var request = new HttpRequestMessage(HttpMethod.Get,
                "http://localhost:5142/api/Division/All/"+allOptionRequired);
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("User-Agent", "HttpClientFactory-Sample");

            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            IList<DivisionViewModel> lisOfDivisions;

            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                lisOfDivisions = JsonConvert.DeserializeObject<IList<DivisionViewModel>>(responseContent);
            }
            else
            {
                lisOfDivisions = Array.Empty<DivisionViewModel>();
            }

            return lisOfDivisions;
        }

        private async Task<DivisionViewModel> GetDivisionByIdAsync(int id)
        {
            var request = new HttpRequestMessage(HttpMethod.Get,
                "http://localhost:5142/api/Division/"+id);
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("User-Agent", "HttpClientFactory-Sample");

            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            DivisionViewModel division;

            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                division = JsonConvert.DeserializeObject<DivisionViewModel>(responseContent);
            }
            else
            {
                division = null;
            }

            return division;
        }
        private async Task<SalaryDetailViewModel> MapToSalaryDetailViewModel(SalaryViewModel salary)
        {
            var officeName = await GetOfficeByIdAsync(salary.IdOffice);
            var divisionName = await GetDivisionByIdAsync(salary.IdDivision);
            var positionName = await GetPositionByIdAsync(salary.IdPosition);

            SalaryDetailViewModel viewModel = new SalaryDetailViewModel
            {
                BaseSalary = salary.BaseSalary,
                BeginDate = salary.BeginDate,
                Birthday = salary.Birthday,
                Commission = salary.Commission,
                CompensationBonus = salary.CompensationBonus,
                Contributions = salary.Contributions,
                DivisionName = divisionName.Name,
                Grade = salary.Grade,
                EmployeeCode = salary.EmployeeCode,
                IdDivision = salary.IdDivision,
                EmployeeName = salary.EmployeeName,
                EmployeeSurname = salary.EmployeeSurname,
                IdentificationNumber = salary.IdentificationNumber,
                IdOffice = salary.IdOffice,
                IdPosition = salary.IdPosition,
                Month = salary.Month,
                OfficeName = officeName.Name,
                PositionName = positionName.Name,
                ProductionBonus = salary.ProductionBonus,
                Year = salary.Year
            };

            return viewModel;
        }
    }
}
